<div class="row">
    <div class="ibox-content" ng-init='setDateFilterAdvanceReport()'>
        <div class="form-group">
            
            <div class="col-lg-9">
                <h3 class="fs25"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp; NEMO GALAPAGOS CRUISES AVAILABILITY DATES</h3>
            </div>
            <div class="col-lg-3">
                    <div class="hiddenElement">
                    <i class="fa fa-ship" aria-hidden="true"></i>&nbsp;<strong>Yacht:</strong>&nbsp;<select name="yacht" ng-disabled="isDisabled" ng-model="selectedYacht.data" ng-change='setDateFilterAdvanceReport(); requestCabins.data = []' ng-options="yacht as yacht.name for yacht in yachts" style="padding:5px 20px;"></select>
                </div>
            </div>
        
        </div>
    </div>
</div>
<div class="project-list" id="tableToExport">
    <table class="table table-bordered tbl-available" style="font-size:12px;">
        <thead  style="font-size:14px;">
            <th class="col-xs-1 text-center" style="padding: 10px 0 15px">CATAMARAN</th>
            <th class="col-xs-2 text-center">DEPARTURES</th>
            <th class="col-xs-1 text-center"  style="padding: 10px 0 15px">ITINERARY</th>
            <th class="col-xs-4 text-center">AVAILABLE CABINS</th>
            <th class="col-xs-2 text-center">SPACES</th>
            <th class="col-xs-2 text-center">ACTIONS</th>
        </thead>
        <tbody>
            <tr class="w33" ng-if="!(row.disabledObj.op || row.departureDisabled)" ng-repeat="row in rows track by $index" id="{{$index}}">
                <td align="left" class="project-title col-xs-2 linea" style="color: #fff;background-color: {{row.color}}; text-align: center; position: relative;">
                    <span class="cell2 cell_title">{{row.yacht}}</span>
                    <span class="cell2 cell_subtitle">{{row.cardinal}} / {{row.diffDays}} Days</span>


                    <div ng-if="row.disabledObj.op || row.departureDisabled" style="position: absolute; left: 0px; top: 0px; background-color: black; opacity: 0.4; width: 100%; height: 100%; z-index:2"></div>
                </td>

                <td align="left" class="project-title col-xs-2 linea" style="min-width: 60px; position: relative; padding-left: 20px">
                    <span class="cell2"><strong>From:</strong> {{row.startDate}}</span>
                    <span class="cell2"><strong>To:</strong> {{row.endDate}}</span>
                    <div ng-if="row.disabledObj.op || row.departureDisabled" style="position: absolute; left: 0px; top: 0px; background-color: black; opacity: 0.4; width: 100%; height: 100%; z-index:2"></div>
                </td>

                <td align="left" class="project-title col-xs-2 linea" style="min-width: 50px; position: relative;text-align:center;">
                    <span class="cell2">{{row.cardinal}} / {{row.diffDays}} Days</span>
                    <span class="cell2">
                        <button data-tooltip="Click here to view" data-toggle="modal" data-target="#myModal5" ng-click="selectItinerary(row.startDate, row.endDate, row.itinerary, row.color, row.yacht, row.itinerary_pic)" style="min-width: 60px;font-size: 12px;background-color: {{row.color}}; border-color: {{row.color}}" type="button" class="btn btn-success btn-xs">{{row.itinerary}}</button>
                    </span>
                    <div ng-if="row.disabledObj.op || row.departureDisabled" style="position: absolute; left: 0px; top: 0px; background-color: black; opacity: 0.4; width: 100%; height: 100%; z-index:2"></div>

                </td>

                <td class="project-title col-xs-4 linea text-center">
                    <div ng-if="!row.disabledObj.op" data-toggle="buttons-checkbox" class="btn-group">
                        <div style="float: left; position: relative" ng-repeat="cabin in row.cabins">

                            <button ng-if="!cabin.superDisabled.op" style="margin-left: 5px; min-width: 40px;" ng-click="selectCabin(cabin, row.cabins.length, row.cruiseId, row.type, row.capacity, row.taken, row.startDate, row.endDate, row.itinerary, row.color, row.yacht, row.yacthCode, row.originalCabins, $parent.$parent.$index)" type="button" class="btn btn-{{cabin.class}}" ng-disabled='cabin.disabled || cabin.disabledDynamic || interval_loading || row.departureDisabled' aria-pressed="false">{{cabin.name}}</button>

                            <button data-tooltip="{{cabin.superDisabled.comment}}" ng-if="cabin.superDisabled.op" style="float: left; margin-left: 5px" type="button" class="btn btn-{{cabin.class}}" ng-disabled="cabin.superDisabled.op" aria-pressed="false">{{cabin.name}}</button>

                            <span ng-if="cabin.numberPax > 0 && cabin.numberPaxClass !== 'warning'" class="badge badge-{{cabin.numberPaxClass}}" style="position: absolute; bottom: -3px; left: 3px; font-size: 9px">{{cabin.numberPax}}</span>

                            <div ng-if="cabin.numberPax > 0 && cabin.numberPaxClass === 'warning' && role !== 'administrador'" class="btn-group" style="position: absolute; bottom: -3px; left: 3px; font-size: 9px; cursor: pointer">
                                <span class="badge badge-{{cabin.numberPaxClass}} dropdown-toggle" aria-expanded="false">{{cabin.numberPax}}</span>
                            </div>

                            <span ng-if="cabin.numberPax2 > 0 && cabin.numberPax2Class !== 'warning'" class="badge badge-{{cabin.numberPax2Class}}" style="position: absolute; bottom: -3px; left: 24px; font-size: 9px">{{cabin.numberPax2}}</span>


                            <div ng-if="cabin.numberPax2 > 0 && cabin.numberPax2Class === 'warning' && role !== 'administrador'" class="btn-group" style="position: absolute; bottom: -3px; left: 3px; font-size: 9px; cursor: pointer">
                                <span class="badge badge-{{cabin.numberPax2Class}} dropdown-toggle" aria-expanded="false">{{cabin.numberPax2}}</span>
                            </div>

                        </div>
                    </div>
                    <div ng-if="row.disabledObj.op" data-toggle="buttons-checkbox" class="btn-group">
                        <div style="float: left; position: relative" ng-repeat="cabin in row.cabins">
                            <button ng-disabled="row.disabledObj.op" style="float: left; margin-left: 5px" type="button" class="btn btn-{{cabin.class}}" aria-pressed="false">{{cabin.name}}</button>
                        </div>
                    </div>
                    <div ng-if="row.disabledObj.op || row.departureDisabled" style="position: absolute; left: 0px; top: 0px; background-color: black; opacity: 0.4; width: 100%; height: 100%; z-index:2"></div>
                    <div ng-if="row.disabledObj.op && !row.departureDisabled"style="position: absolute; left: 20px; top: 35%; opacity: 1; z-index:3; padding-right: 5px"><span style="font-weight: bold; color: white">{{row.disabledObj.comment}}</span></div>
                    <div ng-if="row.departureDisabled"style="position: absolute; left: 20px; top: 35%; opacity: 1; z-index:3; padding-right: 5px"><span style="font-weight: bold; color: white">SOLD OUT</span></div>
                </td>

                <td class="project-title col-xs-2 linea text-center">
                    <span class="badge badge-default text-center">{{row.spaces.available}}</span>&nbsp;<font style="font-size: 11px">available spaces</font><br/>

                    <div ng-if="row.disabledObj.op || row.departureDisabled" style="position: absolute; left: 0px; top: 0px; background-color: black; opacity: 0.4; width: 100%; height: 100%; z-index:2"></div>
                </td>
                <td class="linea text-center">
                    <button ng-if="!row.departureDisabled" data-tooltip="Click here to request" data-toggle="modal" data-target="#myModal4" type="button" ng-click="requestCabins.finish.op = false; requestCabins.finish.msj = ''" class="btn btn-danger btn-sm" ng-disabled="row.request">Request</button>

                    <div ng-if="row.disabledObj.op || row.departureDisabled" style="position: absolute; left: 0px; top: 0px; background-color: black; opacity: 0.4; width: 100%; height: 100%; z-index:2"></div>
                </td>
            </tr>
        </tbody>
    </table>
</div><!--end tabletoExport-->
