<?php
if ($_POST){
	
	// Read the form values
	$success = false;
	$yacht = clean($_POST['yacht']);
	$from = clean($_POST['from']);
	$until = clean($_POST['until']);	
	$itinerary = clean($_POST['itinerary']);	
	$cabins = clean($_POST['cabins']);
	$name = clean($_POST['name']);
	$email = isset( $_POST['email'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['email'] ) : "";
	$country = clean($_POST['country']);
	$countryName = clean($_POST['countryName']);
	$hear = clean($_POST['hear']);
	$payment = clean($_POST['payment']);

	
	if ($name && $email && $yacht && $from && $until && $cabins && $country && $hear && $payment) {
		
		$emailSubject = "Message Received by NemoGalapagosCruises.com";
		$fromName = "Nemo Galapagos Cruises";
		$fromEmail = "maria@nemogalapagoscruises.com";
		
		$to = "=?UTF-8?B?".base64_encode($name)."?= <".$email.">";
		$subject = $emailSubject;
		
		$headers = "From: " . $fromName . " <" . $fromEmail . ">" . "\r\n";
		$headers .= "Reply-To: ". $fromEmail . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		
		$msg  ="<html><body>";
		$msg .= "<p><strong>Dear " . $name .",</strong></p>";
		$msg .= "<p>Many thanks for your email request, we will be delighted to help with ";
		$msg .= "the planning of your perfect trip to Galapagos	Our sales experts will write ";
		$msg .= "back to you just as soon as we can within office hours (Monday to Friday, 9am to 6pm, GMT-5).</p>";
		$msg .= "<p>Submitted Data<br/><br/>";
		$msg .= "<strong>YACHT INFORMATION</strong><br/>";
		$msg .= "<strong>Yacht: </strong>" . $yacht . "<br/>";
		$msg .= "<strong>From: </strong>" . $from . "<strong> Until: </strong>" . $until . "<br/>";
		$msg .= "<strong>Itinerary: </strong>" . $itinerary . "<br/><br/>";
		$msg .= "<strong>Cabins: </strong>" . $cabins . "<br/><br/>";
		$msg .= "<strong>CONTACT INFORMATION</strong><br/>";
		$msg .= "<strong>Name: </strong>" . $name . "<br/>";
		$msg .= "<strong>Email: </strong>" .  $email . "<br/>";
		$msg .= "<strong>Country: </strong>" . $country . ", " . $countryName . "<br/>";
		$msg .= "<strong>How did you hear about us: </strong>" .  $hear . "<br/>";
		$msg .= "<strong>Payment: </strong>" .  $payment . "<br/>";
		$msg .= "</p>";      
		$msg .= "<p><small>Please remember to check your SPAM folder in the following days just ";
		$msg .= "in case their response accidentally ends up there!</small></p>";
		$msg .= "<p>Kind regards,<br>";
		$msg .= "Latintour Cia. Ltda. owner and direct tour operator of Nemo Galapagos fleet</p>";
		$msg .= "</body></html>";
		
		$success = mail( $to, $subject, $msg, $headers );
	
		$ipCliente = getRealIP();
		$emailSubject = "Form from NemoGalapagosCruises.com IP:[". $ipCliente ."]";
		
		$to = $fromName . " <" . $fromEmail . ">";
		$subject = $emailSubject;
		$headers = "From: " . $fromName ." <". $fromEmail .">" . "\r\n";
		$headers .= "Reply-To: " . $name . " <". $email .">" . "\r\n";
		$headers .= "Bcc: nemogalapagos@gmail.com" .  "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html;charset=UTF-8\r\n";
		$msg  ="<html><body>";
		$msg .= "<p>Submitted Data<br/><br/>";
		$msg .= "<strong>YACHT INFORMATION</strong><br/>";
		$msg .= "<strong>Yacht: </strong>" . $yacht . "<br/>";
		$msg .= "<strong>From: </strong>" . $from . "<strong> Until: </strong>" . $until . "<br/>";
		$msg .= "<strong>Itinerary: </strong>" . $itinerary . "<br/><br/>";
		$msg .= "<strong>Cabins: </strong>" . $cabins . "<br/><br/>";
		$msg .= "<strong>CONTACT INFORMATION</strong><br/>";
		$msg .= "<strong>Name: </strong>" . $name . "<br/>";
		$msg .= "<strong>Email: </strong>" .  $email . "<br/>";
		$msg .= "<strong>Country: </strong>" . $country . ", " . $countryName . "<br/>";
		$msg .= "<strong>How did you hear about us: </strong>" .  $hear . "<br/>";
		$msg .= "<strong>Payment: </strong>" .  $payment . "<br/>";
		$msg .= "</p>";      
		$msg .= "</body></html>";
		$success = mail( $to, $subject, $msg, $headers );
	}
	// Return an appropriate response to the browser
	echo $success ? "success" : "error";
}// end_POST

function getRealIP() {
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
		
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	
	return $_SERVER['REMOTE_ADDR'];
}

function clean($str)
{
	$str = is_array($str) ? array_map(array("ProcessForm", 'clean'), $str) : str_replace('\\', '\\\\', strip_tags(trim(htmlspecialchars((get_magic_quotes_gpc() ? stripslashes($str) : $str), ENT_QUOTES))));
    return $str;
}

?>