<!DOCTYPE html>
<html lang="en">
<head>

    <!--Meta Codificación de caracteres-->
    <meta charset="utf-8">

    <!--Meta Compatibilidad-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Meta Responsive-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Título del Sitio-->
    <title>Nemo Galapagos Islands Cruises | Tours in Galapagos Islands</title>

    <!--Meta Descripción del Sitio-->
    <meta name="description" content="NEMO GALAPAGOS CRUISES is a family business that has been sailing in the Galapagos
	Islands since 1985. We offer first class cruises to the Galapagos Islands. We own three boats available for cruises
	to the Galapagos Islands; Nemo I, Nemo II, Nemo III.  We offer Tours in Galapagos Island."/>

    <!--Meta Palabras Claves-->
    <meta name="keywords" content="Galapagos islands cruises, Nemo Galapagos islands cruises, Galapagos Islands Travel,
	Galapagos last minute cruises, Galapagos cruise deals, Galapagos Islands Tours."/>

    <!--Meta Autor-->
    <meta name="author" content="Latintour Cia. Ltda." />

    <!--Meta Publicación-->
    <meta name="publisher" content="Latintour Cia. Ltda." />

    <!--Meta Derechos Intelectuales del Sitio-->
    <meta name="copyright" content="Latintour Cia. Ltda." />

    <!--Meta Distribución-->
    <meta name=”distribution” content=”global”/>

    <!--Meta Robots-->
    <meta name="robots" content="index, follow" />

    <!--Meta Canonical (Evita Duplicado)-->
    <link rel=”canonical” href=”https://nemogalapagoscruises.com/” />

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="76x76" href="img/logos/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/logos/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/logos/favicon/favicon-16x16.png">
    <!--<link rel="manifest" href="img/logos/favicon/site.webmanifest">-->
    <link rel="mask-icon" href="img/logos/favicon/safari-pinned-tab.svg" color="#0c345c">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- link to font awesome -->
    <link media="all" rel="stylesheet" href="vendors/font-awesome/css/font-awesome.css">
    <!-- link to custom icomoon fonts -->
    <link rel="stylesheet" type="text/css" href="css/fonts/icomoon/icomoon.css">
    <!-- link to wow js animation -->
    <link media="all" rel="stylesheet" href="vendors/animate/animate.css">
    <!-- include bootstrap css -->
    <link media="all" rel="stylesheet" href="css/bootstrap.css">
    <!-- include owl css -->
    <link media="all" rel="stylesheet" href="vendors/owl-carousel/owl.carousel.css">
    <link media="all" rel="stylesheet" href="vendors/owl-carousel/owl.theme.css">
    <!-- link to revolution css  -->
    <link rel="stylesheet" type="text/css" href="vendors/revolution/css/settings.css">
    <!-- include main css -->
    <link media="all" rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="preloader" id="pageLoad">

    <div class="holder">
        <!--<div class="coffee_cup"></div> -->
    </div>
</div>


<!-- main wrapper -->
<div id="wrapper">
    <div class="page-wrapper">
        <!-- main header -->
        <header id="header">
            <div class="container-fluid">
                <!-- logo -->
                <div class="logo">
                    <a href="index.html">
                        <img class="normal" src="img/logos/logo-latin-tour.png" alt="Logo Nemo Galapagos Cruises">
                    </a>
                </div>
                <!-- main navigation -->
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle nav-opener" data-toggle="collapse" data-target="#nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- main menu items and drop for mobile -->
                    <div class="collapse navbar-collapse" id="nav">
                        <!-- main navbar -->
                        <ul class="nav navbar-nav">
                            <!--INICIO-->
                            <li class="dropdown">
                                <a href="index.html">Home</a>
                            </li>
                            <!--FLOTA NEMO-->
                            <li class="dropdown has-mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">NEMO FLEET <b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu">
                                    <div class="drop-wrap">
                                        <div class="drop-holder">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="col">
                                                        <div class="img-wrap">
                                                            <a href="nemo-I.html"><img src="img/generic/nemo1.jpg" height="175" width="350" alt="image description"></a>
                                                        </div>
                                                        <div class="des">
                                                            <strong class="title"><a href="nemo-I.html">NEMO I</a></strong>
                                                            <p>With a length of 24.90 meters and a width of 10 meters, this Catamaran reaches 10 knots and accommodates up to 14 guests in comfortable cabins.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="col">
                                                        <div class="img-wrap">
                                                            <a href="nemo-II.php"><img src="img/generic/nemo2.jpg" height="175" width="370" alt="image description"></a>
                                                        </div>
                                                        <div class="des">
                                                            <strong class="title"><a href="nemo-II.php">NEMO II</a></strong>
                                                            <p>With a length of 21.88 meters and a width of 10.39 meters, this Catamaran reaches 10 knots and accommodates up to 14 guests in comfortable cabins.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="col">
                                                        <div class="img-wrap">
                                                            <a href="nemo-III.php"><img src="img/generic/nemo3.jpg" height="175" width="370" alt="image description"></a>
                                                        </div>
                                                        <div class="des">
                                                            <strong class="title"><a href="nemo-III.php">NEMO III</a></strong>
                                                            <p>With a length of 23 meters and a width of 11.40 meters, this Catamaran reaches 10 knots and accommodates up to 16 guests in comfortable cabins.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--VISITA LA ISLA-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Island Hopping <b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu mt-0">
                                    <ul>
                                        <li><a href="galapagos-island-4-days.html">GALAPAGOS ISLAND HOPPING 4 DAYS</a></li>
                                        <li><a href="galapagos-inter-islands-5-days.html">GALAPAGOS ISLAND HOPPING 5 DAYS</a></li>
                                    </ul>
                                </div>
                            </li>
                            <!--TOURS-->
                            <li class="dropdown has-mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mainland Tours<b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu">
                                    <div class="drop-wrap">
                                        <div class="five-col">
                                            <div class="column">
                                                <strong class="title sub-link-opener">Ecuador - Andes</strong>
                                                <ul class="header-link">
                                                    <li><a href="cotopaxi.html">COTOPAXI</a></li>
                                                    <li><a href="ecuador-by-train.html">ECUADOR BY TRAIN</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">Ecuador - Amazon</strong>
                                                <ul class="header-link">
                                                    <li><a href="napo-wildlife.html">NAPO WILDLIFE CENTER</a></li>
                                                    <li><a href="cuyabeno-lodge.html">CUYABENO LODGE</a></li>
                                                    <li><a href="sacha-lodge.html">SACHA LODGE</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">Ecuador - Day Trips</strong>
                                                <ul class="header-link">
                                                    <li><a href="indigenous-market.html">INDIGENOUS MARKETS</a></li>
                                                    <li><a href="quito-city.html">QUITO CITY TOUR AND MIDDLE OF THE WORLD</a></li>
                                                    <li><a href="mindo.html">MINDO CLOUD FOREST</a></li>
                                                    <li><a href="papallacta-thermas.html">PAPALLACTA THERMAS & ROSE FARM</a></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">Perú</strong>
                                                <ul class="header-link">
                                                    <li><a href="machupicchu-6days.html">LIMA-CUSCO-MACHUPICCHU</a></li>
                                                    <li><a href="machupicchu-5days.html">CUSCO - MACHUPICCHU 5 DAYS</a></li>
                                                    <li><a href="machupicchu-4days.html">CUSCO - MACHUPICCHU 4 DAYS</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">BOLÍVIA</strong>
                                                <ul class="header-link">
                                                    <li><a href="bolivia-4days.html">BOLIVIA AT GLANCE 4 DAYS</a></li>
                                                    <li><a href="bolivia-5days.html">BOLIVIA COMFORTABLE 5 DAYS</a></li>
                                                    <li><a href="bolivia-6days.html">BOLIVIA MAJESTIC 6 DAYS</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--OFFER-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Special Offers <b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu mt-0">
                                    <ul>
                                        <li><a href="#">PACKAGE GALAPAGOS + PAPALLACTA HOT SPRINGS</a></li>
                                        <li><a href="#">EARLY BIRD GALAPAGOS CRUISES 2019</a></li>
                                    </ul>
                                </div>
                            </li>
                            <!--ABOUT US-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="icon-angle-down"></b></a>
                            </li>
                            <!--CONTACT US-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact Us  <b class="icon-angle-down"></b></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

        </header>

			<!-- main container -->
			<main id="main">
				<!-- main tour information -->
				<section class="container-fluid trip-info">
					<div class="same-height row">
						<div class="height col-md-12">
							<!-- top image slideshow -->
							<div id="tour-slide">
								<div class="slide">
									<div class="bg-stretch">
										<img src="img/nemoII/slider/nemo2-1.jpg" title="Nemo II" alt="Nemo II" height="933" width="1400">
									</div>
								</div>
								<div class="slide">
									<div class="bg-stretch">
										<img src="img/nemoIII/slider/nemo3-3.jpg" title="Nemo III" alt="Nemo III" height="933" width="1400">
									</div>
								</div>

							</div>
						</div>

					</div>
				</section>
				<div class="tab-container">
                    <h2 class="mt-50 text-center">NEMO GALAPAGOS CRUISES</h2>
                    <div class="page-content">
                        <div class="container">
                            <div id="available" class="ibox" ng-app="AvailableApp">
                                <div class="ibox-content" ng-if="yachts !== ''" ng-controller="mainController">
                                    <?php
                                    include_once "php/table-all-nemo-availability.php";
                                    include_once "php/form-modal-availability.php";
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</main>
		</div>




	<!--FOOTER -->
    <footer id="footer">
        <div class="container">
            <!-- newsletter form -->
            <form action="php/subscribe.php" id="signup" method="post" class="newsletter-form">
                <fieldset>
                    <div class="input-holder">
                        <input type="email" class="form-control" placeholder="Email Address" name="subscriber_email" id="subscriber_email">
                        <input type="submit" value="GO">
                    </div>
                    <span class="info" id="subscribe_message">To receive news, updates and tour packages via email.</span>
                </fieldset>
            </form>
            <!-- footer links -->
            <div class="row footer-holder">
                <nav class="col-sm-4 col-lg-3 footer-nav active">
                    <h3>FLEET</h3>
                    <ul class="slide">
                        <li><a href="nemo-I.html">Nemo I</a></li>
                        <li><a href="#">Nemo II</a></li>
                        <li><a href="#">Nemo III</a></li>

                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav">
                    <h3>Destinations</h3>
                    <ul class="slide">
                        <li><a href="#">Galapago Islands</a></li>
                        <li><a href="#">Ecuador Andes</a></li>
                        <li><a href="#">Ecuador Amazon</a></li>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav">
                    <h3>Other Destinations</h3>
                    <ul class="slide">
                        <li><a href="#">Perú - MachuPicchu</a></li>
                        <li><a href="#">Perú - Lima</a></li>
                        <li><a href="#">Bolivia</a></li>
                    </ul>
                </nav>
                <!--<nav class="col-sm-4 col-lg-2 footer-nav">
                    <h3>reservation</h3>
                    <ul class="slide">
                        <li><a href="#">Booking Conditions</a></li>
                        <li><a href="#">My Bookings</a></li>
                        <li><a href="#">Refund Policy</a></li>
                        <li><a href="#">Includes &amp; Excludes</a></li>
                        <li><a href="#">Your Responsibilities</a></li>
                        <li><a href="#">Order a Brochure</a></li>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-2 footer-nav">
                    <h3>ask Entrada</h3>
                    <ul class="slide">
                        <li><a href="#">Why Entrada?</a></li>
                        <li><a href="#">Ask an Expert</a></li>
                        <li><a href="#">Safety Updates</a></li>
                        <li><a href="#">We Help When...</a></li>
                        <li><a href="#">Personal Matters</a></li>
                    </ul>
                </nav>-->
                <nav class="col-sm-4 col-lg-3 footer-nav last">
                    <h3>contact LatinTour</h3>
                    <ul class="slide address-block">
                        <li class="wrap-text"><span class="icon-tel"></span> <a href="#">+1 305 848 7326</a></li>
                        <li class="wrap-text"><span class="icon-tel"></span> <a href="#">+1 604 243 9774</a></li>
                        <li class="wrap-text"><span class="icon-tel"></span> <a href="#">+593 2 2 508 800</a></li>
                        <!--<li class="wrap-text"><span class="icon-tel"></span> <a href="tel:02072077878">+593 2 2 508 800</a></li>-->
                        <li><span class="icon-home"></span> <address>DIEGO DE ALMAGRO N26 - 205 AND LA NINA
                                QUITO EC170135</address></li>
                    </ul>
                </nav>
            </div>
            <!-- social wrap -->
            <ul class="social-wrap">
                <li class="facebook"><a target="_blank" href="https://www.facebook.com/NemoGalapagosCruises/timeline/">
                        <span class="icon-facebook"></span>
                        <strong class="txt">Like Us</strong>
                    </a></li>
                <li class="twitter"><a target="_blank" href="https://twitter.com/NemoGalapagos">
                        <span class="icon-twitter"></span>
                        <strong class="txt">Follow On</strong>
                    </a></li>
                <li class="google-plus"><a target="_blank" href="https://www.youtube.com/user/nemogalapagos">
                        <span class="fa-icon-google-plus"></span>
                        <strong class="txt">Like Us</strong>
                    </a></li>
                <li class="vimeo"><a href="#">
                        <span class="icon-vimeo"></span>
                        <strong class="txt">Share At</strong>
                    </a></li>
                <li class="pin"><a target="_blank" href="https://www.pinterest.com/NemoGalapagos/">
                        <span class="icon-pin"></span>
                        <strong class="txt">Pin It</strong>
                    </a></li>
                <!--<li class="dribble"><a href="#">
                    <span class="icon-dribble"></span>
                    <strong class="txt">Dribbble</strong>
                </a></li>-->
            </ul>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- copyright -->
                        <strong class="copyright"><i class="fa fa-copyright"></i> Copyright 2019 - Nemo Galapagos Cruises - Latintour Cia. Ltda.</strong>
                    </div>
                    <div class="col-lg-6">
                        <ul class="payment-option">
                            <li>
                                <img src="img/footer/visa.png" alt="visa">
                            </li>
                            <li>
                                <img src="img/footer/mastercard.png" height="20" width="33" alt="master card">
                            </li>
                            <!--<li>
                                <img src="img/footer/paypal.png" height="20" width="72" alt="paypal">
                            </li>
                            <li>
                                <img src="img/footer/maestro.png" height="20" width="33" alt="maestro">
                            </li>-->
                            <li>
                                <img src="img/footer/bank-transfer.png" height="20" width="55" alt="bank transfer">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	</div>
	<!-- scroll to top -->
	<div class="scroll-holder text-center">
		<a href="javascript:" id="scroll-to-top"><i class="icon-arrow-down"></i></a>
	</div>

    <div class="slide-out-div"> <a class="handle">Contact Us</a>
    <!-- Contact form-->
    <div class="nemo-contact">
        <p class="medium white bold linea">Contact Form</p>
        <form action="processForm.php" class="form-vertical" id="formContactUs" method="post" novalidate>
            <input name="n1" type="hidden" id="n1" />
            <input name="n2" type="hidden" id="n2" />
            <input name="nameCountry" type="hidden" id="nameCountry" value="" />
            <!-- Name -->
            <div class="form-group">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" required maxlength="40">
            </div>
            <!-- Email -->
            <div class="form-group">
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required maxlength="50">
            </div>
            <!-- Country -->
            <div class="form-group">
                <select id="country" class="form-control" name="country" onChange="jsFunction();">
                    <option value="">Select a Country</option>
                    <option value="AF">Afghanistan</option>
                    <option value="AX">Aland Islands</option>
                    <option value="AL">Albania</option>
                    <option value="DZ">Algeria</option>
                    <option value="AS">American Samoa</option>
                    <option value="AD">Andorra</option>
                    <option value="AO">Angola</option>
                    <option value="AI">Anguilla</option>
                    <option value="AQ">Antarctica</option>
                    <option value="AG">Antigua and Barbuda</option>
                    <option value="AR">Argentina</option>
                    <option value="AM">Armenia</option>
                    <option value="AW">Aruba</option>
                    <option value="AP">Asia/Pacific Region</option>
                    <option value="AU">Australia</option>
                    <option value="AT">Austria</option>
                    <option value="AZ">Azerbaijan</option>
                    <option value="BS">Bahamas</option>
                    <option value="BH">Bahrain</option>
                    <option value="BD">Bangladesh</option>
                    <option value="BB">Barbados</option>
                    <option value="BY">Belarus</option>
                    <option value="BE">Belgium</option>
                    <option value="BZ">Belize</option>
                    <option value="BJ">Benin</option>
                    <option value="BM">Bermuda</option>
                    <option value="BT">Bhutan</option>
                    <option value="BO">Bolivia</option>
                    <option value="BQ">Bonaire, Saint Eustatius and Saba</option>
                    <option value="BA">Bosnia and Herzegovina</option>
                    <option value="BW">Botswana</option>
                    <option value="BV">Bouvet Island</option>
                    <option value="BR">Brazil</option>
                    <option value="IO">British Indian Ocean Territory</option>
                    <option value="BN">Brunei Darussalam</option>
                    <option value="BG">Bulgaria</option>
                    <option value="BF">Burkina Faso</option>
                    <option value="BI">Burundi</option>
                    <option value="KH">Cambodia</option>
                    <option value="CM">Cameroon</option>
                    <option value="CA">Canada</option>
                    <option value="CV">Cape Verde</option>
                    <option value="KY">Cayman Islands</option>
                    <option value="CF">Central African Republic</option>
                    <option value="TD">Chad</option>
                    <option value="CL">Chile</option>
                    <option value="CN">China</option>
                    <option value="CX">Christmas Island</option>
                    <option value="CC">Cocos (Keeling) Islands</option>
                    <option value="CO">Colombia</option>
                    <option value="KM">Comoros</option>
                    <option value="CG">Congo</option>
                    <option value="CD">Congo, The Democratic Republic of the</option>
                    <option value="CK">Cook Islands</option>
                    <option value="CR">Costa Rica</option>
                    <option value="CI">Cote d'Ivoire</option>
                    <option value="HR">Croatia</option>
                    <option value="CU">Cuba</option>
                    <option value="CW">Curacao</option>
                    <option value="CY">Cyprus</option>
                    <option value="CZ">Czech Republic</option>
                    <option value="DK">Denmark</option>
                    <option value="DJ">Djibouti</option>
                    <option value="DM">Dominica</option>
                    <option value="DO">Dominican Republic</option>
                    <option value="EC">Ecuador</option>
                    <option value="EG">Egypt</option>
                    <option value="SV">El Salvador</option>
                    <option value="GQ">Equatorial Guinea</option>
                    <option value="ER">Eritrea</option>
                    <option value="EE">Estonia</option>
                    <option value="ET">Ethiopia</option>
                    <option value="EU">Europe</option>
                    <option value="FK">Falkland Islands (Malvinas)</option>
                    <option value="FO">Faroe Islands</option>
                    <option value="FJ">Fiji</option>
                    <option value="FI">Finland</option>
                    <option value="FR">France</option>
                    <option value="GF">French Guiana</option>
                    <option value="PF">French Polynesia</option>
                    <option value="TF">French Southern Territories</option>
                    <option value="GA">Gabon</option>
                    <option value="GM">Gambia</option>
                    <option value="GE">Georgia</option>
                    <option value="DE">Germany</option>
                    <option value="GH">Ghana</option>
                    <option value="GI">Gibraltar</option>
                    <option value="GR">Greece</option>
                    <option value="GL">Greenland</option>
                    <option value="GD">Grenada</option>
                    <option value="GP">Guadeloupe</option>
                    <option value="GU">Guam</option>
                    <option value="GT">Guatemala</option>
                    <option value="GG">Guernsey</option>
                    <option value="GN">Guinea</option>
                    <option value="GW">Guinea-Bissau</option>
                    <option value="GY">Guyana</option>
                    <option value="HT">Haiti</option>
                    <option value="HM">Heard Island and McDonald Islands</option>
                    <option value="VA">Holy See (Vatican City State)</option>
                    <option value="HN">Honduras</option>
                    <option value="HK">Hong Kong</option>
                    <option value="HU">Hungary</option>
                    <option value="IS">Iceland</option>
                    <option value="IN">India</option>
                    <option value="ID">Indonesia</option>
                    <option value="IR">Iran, Islamic Republic of</option>
                    <option value="IQ">Iraq</option>
                    <option value="IE">Ireland</option>
                    <option value="IM">Isle of Man</option>
                    <option value="IL">Israel</option>
                    <option value="IT">Italy</option>
                    <option value="JM">Jamaica</option>
                    <option value="JP">Japan</option>
                    <option value="JE">Jersey</option>
                    <option value="JO">Jordan</option>
                    <option value="KZ">Kazakhstan</option>
                    <option value="KE">Kenya</option>
                    <option value="KI">Kiribati</option>
                    <option value="KP">Korea, Democratic People's Republic of</option>
                    <option value="KR">Korea, Republic of</option>
                    <option value="KW">Kuwait</option>
                    <option value="KG">Kyrgyzstan</option>
                    <option value="LA">Lao People's Democratic Republic</option>
                    <option value="LV">Latvia</option>
                    <option value="LB">Lebanon</option>
                    <option value="LS">Lesotho</option>
                    <option value="LR">Liberia</option>
                    <option value="LY">Libyan Arab Jamahiriya</option>
                    <option value="LI">Liechtenstein</option>
                    <option value="LT">Lithuania</option>
                    <option value="LU">Luxembourg</option>
                    <option value="MO">Macao</option>
                    <option value="MK">Macedonia</option>
                    <option value="MG">Madagascar</option>
                    <option value="MW">Malawi</option>
                    <option value="MY">Malaysia</option>
                    <option value="MV">Maldives</option>
                    <option value="ML">Mali</option>
                    <option value="MT">Malta</option>
                    <option value="MH">Marshall Islands</option>
                    <option value="MQ">Martinique</option>
                    <option value="MR">Mauritania</option>
                    <option value="MU">Mauritius</option>
                    <option value="YT">Mayotte</option>
                    <option value="MX">Mexico</option>
                    <option value="FM">Micronesia, Federated States of</option>
                    <option value="MD">Moldova, Republic of</option>
                    <option value="MC">Monaco</option>
                    <option value="MN">Mongolia</option>
                    <option value="ME">Montenegro</option>
                    <option value="MS">Montserrat</option>
                    <option value="MA">Morocco</option>
                    <option value="MZ">Mozambique</option>
                    <option value="MM">Myanmar</option>
                    <option value="NA">Namibia</option>
                    <option value="NR">Nauru</option>
                    <option value="NP">Nepal</option>
                    <option value="NL">Netherlands</option>
                    <option value="NC">New Caledonia</option>
                    <option value="NZ">New Zealand</option>
                    <option value="NI">Nicaragua</option>
                    <option value="NE">Niger</option>
                    <option value="NG">Nigeria</option>
                    <option value="NU">Niue</option>
                    <option value="NF">Norfolk Island</option>
                    <option value="MP">Northern Mariana Islands</option>
                    <option value="NO">Norway</option>
                    <option value="OM">Oman</option>
                    <option value="PK">Pakistan</option>
                    <option value="PW">Palau</option>
                    <option value="PS">Palestinian Territory</option>
                    <option value="PA">Panama</option>
                    <option value="PG">Papua New Guinea</option>
                    <option value="PY">Paraguay</option>
                    <option value="PE">Peru</option>
                    <option value="PH">Philippines</option>
                    <option value="PN">Pitcairn</option>
                    <option value="PL">Poland</option>
                    <option value="PT">Portugal</option>
                    <option value="PR">Puerto Rico</option>
                    <option value="QA">Qatar</option>
                    <option value="RE">Reunion</option>
                    <option value="RO">Romania</option>
                    <option value="RU">Russian Federation</option>
                    <option value="RW">Rwanda</option>
                    <option value="BL">Saint Bartelemey</option>
                    <option value="SH">Saint Helena</option>
                    <option value="KN">Saint Kitts and Nevis</option>
                    <option value="LC">Saint Lucia</option>
                    <option value="MF">Saint Martin</option>
                    <option value="PM">Saint Pierre and Miquelon</option>
                    <option value="VC">Saint Vincent and the Grenadines</option>
                    <option value="WS">Samoa</option>
                    <option value="SM">San Marino</option>
                    <option value="ST">Sao Tome and Principe</option>
                    <option value="SA">Saudi Arabia</option>
                    <option value="SN">Senegal</option>
                    <option value="RS">Serbia</option>
                    <option value="SC">Seychelles</option>
                    <option value="SL">Sierra Leone</option>
                    <option value="SG">Singapore</option>
                    <option value="SX">Sint Maarten</option>
                    <option value="SK">Slovakia</option>
                    <option value="SI">Slovenia</option>
                    <option value="SB">Solomon Islands</option>
                    <option value="SO">Somalia</option>
                    <option value="ZA">South Africa</option>
                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                    <option value="SS">South Sudan</option>
                    <option value="ES">Spain</option>
                    <option value="LK">Sri Lanka</option>
                    <option value="SD">Sudan</option>
                    <option value="SR">Suriname</option>
                    <option value="SJ">Svalbard and Jan Mayen</option>
                    <option value="SZ">Swaziland</option>
                    <option value="SE">Sweden</option>
                    <option value="CH">Switzerland</option>
                    <option value="SY">Syrian Arab Republic</option>
                    <option value="TW">Taiwan</option>
                    <option value="TJ">Tajikistan</option>
                    <option value="TZ">Tanzania, United Republic of</option>
                    <option value="TH">Thailand</option>
                    <option value="TL">Timor-Leste</option>
                    <option value="TG">Togo</option>
                    <option value="TK">Tokelau</option>
                    <option value="TO">Tonga</option>
                    <option value="TT">Trinidad and Tobago</option>
                    <option value="TN">Tunisia</option>
                    <option value="TR">Turkey</option>
                    <option value="TM">Turkmenistan</option>
                    <option value="TC">Turks and Caicos Islands</option>
                    <option value="TV">Tuvalu</option>
                    <option value="UG">Uganda</option>
                    <option value="UA">Ukraine</option>
                    <option value="AE">United Arab Emirates</option>
                    <option value="GB">United Kingdom</option>
                    <option value="US">United States</option>
                    <option value="UM">United States Minor Outlying Islands</option>
                    <option value="UY">Uruguay</option>
                    <option value="UZ">Uzbekistan</option>
                    <option value="VU">Vanuatu</option>
                    <option value="VE">Venezuela</option>
                    <option value="VN">Vietnam</option>
                    <option value="VG">Virgin Islands, British</option>
                    <option value="VI">Virgin Islands, U.S.</option>
                    <option value="WF">Wallis and Futuna</option>
                    <option value="EH">Western Sahara</option>
                    <option value="YE">Yemen</option>
                    <option value="ZM">Zambia</option>
                    <option value="ZW">Zimbabwe</option>
                </select>
            </div>
            <script type="text/javascript">
                function jsFunction(){
                    var myselect = document.getElementById("country");
                    document.getElementById("nameCountry").value = myselect.options[myselect.selectedIndex].text;
                }
            </script>
            <!-- Message -->
            <div class="form-group">
                <textarea class="form-control" id="message" name="message" rows="3" placeholder="Message" ></textarea>
            </div>
            <!-- Captcha-->
            <div class="form-group">
                <div class="white" style="padding-bottom:5px;">Math Captcha</div>
                <input type="text" class="form-control" id="expect" name="expect" placeholder="Your answer">
            </div>
            <!-- Buttons -->
            <div class="form-group">
                <button type="submit" class="btn btn-default" rel="nofollow" id="sendButton" title="Send Email">Send Email</button>
            </div>
        </form>
    </div>
    <div style="margin-top:10px;"></div>
    <div class="nemo-phone">
        <p class="medium white bold linea">Call NEMO owners</p>
        <ul>
            <li class="usFlag parpadea"><strong><a href="tel:+13058487326">+1 305 848 7326</a></strong></li>
            <li class="caFlag parpadea"><strong><a href="tel:+16042439774">+1 604 243 9774</a></strong></li>
            <li class="ecFlag"><a href="tel:+59322508800">+593 2 2 508 800</a> / 10 / 11</li>
        </ul>
    </div>

    <div class="nemo-emails">
        <p class="medium white bold linea"></p>
        <ul>
            <li class="emailFlag"><a href="mailto:maria@nemogalapagoscruises.com">maria@nemogalapagoscruises.com</a></li>
            <li class="skypeFlag"><a href="skype:nemogalapagos">nemogalapagos</a></li>
        </ul>
    </div>
    <div class="nemo-office">Office hours are 9am to 6pm. Monday<br/>
        to Friday, GMT-5 eastern time.</div>
    <div id="sendingMessage" class="statusMessage">
        <p>Sending your message. Please wait...</p>
    </div>
    <div id="successMessage" class="statusMessage">
        <p>Thanks for sending your message! We'll get back to you shortly.</p>
    </div>
    <div id="failureMessage" class="statusMessage">
        <p>There was a problem sending your message. Please try again.</p>
    </div>
    <div id="incompleteMessage" class="statusMessage">
        <p>Please complete all the fields in the form before sending.</p>
    </div>
</div>

	<!-- jquery library -->
	<script src="vendors/jquery/jquery-2.1.4.min.js"></script>
	<!-- external scripts -->
	<script src="vendors/bootstrap/javascripts/bootstrap.min.js"></script>
	<script src="vendors/jquery-placeholder/jquery.placeholder.min.js"></script>
	<script src="vendors/match-height/jquery.matchHeight.js"></script>
	<script src="vendors/wow/wow.min.js"></script>
	<script src="vendors/stellar/jquery.stellar.min.js"></script>
	<script src="vendors/validate/jquery.validate.js"></script>
	<script src="vendors/waypoint/waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="vendors/jquery-ui/jquery-ui.min.js"></script>
	<script src="vendors/jQuery-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<script src="vendors/fancybox/jquery.fancybox.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="vendors/jcf/js/jcf.js"></script>
	<script src="vendors/jcf/js/jcf.select.js"></script>
	<script src="vendors/retina/retina.min.js"></script>
	<script src="vendors/bootstrap-datetimepicker-master/dist/js/bootstrap-datepicker.js"></script>
	<script src="vendors/sticky-kit/sticky-kit.js"></script>
	<!-- mailchimp newsletter subscriber -->
	<script src="js/mailchimp.js"></script>
	<!--INTEGRATION-->
	<script src="js/integration/angular.min.js"></script>
	<script src="js/integration/inicio.js"></script>
	<!-- custom script -->
	<script src="js/sticky-kit-init.js"></script>
	<script src="js/jquery.main.js"></script>
</body>
</html>