<?php
function printAvailability($y, $i, $p, $h){
			$yacth = $y;
			$itinerary = $i;
			$part = $p;
			$howMany = $h;
			$url = "http://45.79.111.123/api.php?yacht=" . $yacth . "&itinerary=" . $itinerary . "&part=" . $part . "&howMany=" . $howMany;
			$ng = ($yacth == "nemo2")?"II":"III";
			
			$client = curl_init($url);
			curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($client);
			$result = json_decode($response, true);
			if (isset($result['status']) && $result['status'] === 200) {
				$startYear ="";
                foreach ($result['data'] as $key => $value) {
                	if ($value['comment'] !="N/A"){
						$currentYear = gmdate("Y", $value['start_date']);
						if ($currentYear != $startYear){
							if ($startYear!=""){
                            	$str  = '</table>';
								$str .= '<div class="theme-color txt-xl text-center"><a href="book-nemo-galapagos-cruises.php#form" class="btn btn-primary btn-lg" rel="nofollow" title="Contact us for more information Nemo '. $ng .' Galapagos Cruises"><i class="fa fa-info-circle" aria-hidden="true"></i> Contact us for more information</a></div>';
                                echo $str;

                            }
                            echo '<h4>AVAILABLE DATES '. $currentYear . '</h4>';
                            $str = '<table class="table table-hover">';
                            $str .= "<tr>";
                            $str .= "<th>AVAILABLE DATES</th>";
                            $str .= "<th>DURATION</th>";
                            $str .= "<th>SPACES</th>";
                            $str .= "<th></th>";
                            $str .= "</tr>";

                            $startYear = $currentYear;
                            echo $str;
                       }
					   $nitinerary = ($itinerary=="A")?"North":"South";
                       $pItinerary = $value['itinerary']['short'];
                       if ($pItinerary == $itinerary. "8") {$d = 8; $n = ($yacth == "nemo2" && $itinerary == "A")?10:1;}
                       elseif ($pItinerary == $itinerary. "5") {$d = 5; $n = 2;}
                       else {$d = 4; $n = 3;}

                       $str = '<tr class="n'.$n.'">';
                       $str .= "<td>".gmdate("M j", $value['start_date'])." - ".gmdate("M j Y", $value['end_date'])."</td>";
                       $str .= '<td><span class="badge badge-'.$d.'d"><a href="#modalItinerary' . $pItinerary . '" data-toggle="modal" title="'. $nitinerary .' Itinerary '.$d.' Days" rel="nofollow">'.$pItinerary.'</a></span></td>';

                       if  (!$value['disabled']){
						   $str .= "<td>".$value['availability']."</td>";
                           $str .= "<td><a href='book-nemo-galapagos-cruises.php?arrival=".gmdate('d-m-Y',$value['start_date'])."&departure=".gmdate('d-m-Y',$value['end_date'])."#form' title='Book Now on ".$value['yacht_name']." Galapagos Cruises - ".gmdate('F j',$value['start_date'])." at ".gmdate('F j - Y',$value['end_date'])."'>Book now!</a></td>";
					   }else{
						   $str .= '<td colspan="2">'.$value['comment'].'</td>';
                       }    

                       $str .= "</tr>";
                       echo $str;
					}
				}
				echo '</table>';
			}
			echo '<div class="theme-color txt-xl text-center"><a href="book-nemo-galapagos-cruises.php#form" class="btn btn-primary btn-lg" rel="nofollow" title="Contact us for more information Nemo '. $ng .' Galapagos Cruises"><i class="fa fa-info-circle" aria-hidden="true"></i> Contact us for more information</a></div>';
		}
		
function printEarlyBird($y, $i, $p, $h, $o, $a){
                            $yacth = $y;
                            $itinerary = $i;
                            $part = $p;
                            $howMany = $h;
                            $offer = $o;
							$currentYear = $a;
							$ng = ($yacth == "nemo2")?"II":"III";
							 
                            $url = "http://45.79.111.123/offers.php?yacht=" . $yacth . "&itinerary=" . $itinerary . "&part=" . $part . "&howMany=" . $howMany . "&offer=" . $offer;

                            $client = curl_init($url);
                            curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
                            $response = curl_exec($client);
                            $result = json_decode($response, true);

                            if (isset($result['status']) && $result['status'] === 200) {

                                $str = '<table class="table table-hover">';
                                $str .= "<tr>";
                                $str .= "<th>AVAILABLE DATES</th>";
                                $str .= "<th>DURATION</th>";
                                $str .= "<th>SPACES</th>";
                                $str .= "<th></th>";
                                $str .= "</tr>";
                                echo $str;

                                foreach ($result['data'] as $key => $value) {
                                    if ($value['comment'] !="N/A" && $value['earlyBird']  && gmdate("Y", $value['start_date'])== $currentYear){

                                        $pItinerary = $value['itinerary']['short'];
                                        $nItinerary = (substr($pItinerary,0,1) == "A")?"North ":"South ";                                        
                                        $d = substr($pItinerary,1,1);
                                        
                                        if ($d == 8){$n = 1;}
                                        elseif ($d == 5) {$n = 2;}
                                        else {$n = 3;}

                                        
                                        $myModal = "modalItinerary('". $nItinerary . " Itinerary " . $pItinerary . " - ". $d .  " Days','img/nemo-". strtolower($ng) ."-availability/" . trim(strtolower($nItinerary)) . "-itinerary-" .$d ."-days-nemo-". strtolower($ng) ."-galapagos-cruise.jpg');";

                                        $str = '<tr class="n'.$n.'">';
                                        $str .= "<td>".gmdate("M j", $value['start_date'])." - ".gmdate("M j Y", $value['end_date'])."</td>";
                                        $str .= '<td><span class="badge badge-'.$d.'d"><a href="#" data-toggle="modal" onclick="'. $myModal . '" title="' . $nItinerary . ' Itinerary ' . $d.' Days" rel="nofollow">'. $pItinerary . '</a></span></td>';

                                        if  (!$value['disabled']){
                                            $str .= "<td>".$value['availability']."</td>";
                                            $str .= "<td><a href='book-nemo-galapagos-cruises.php?arrival=".gmdate('d-m-Y',$value['start_date'])."&departure=".gmdate('d-m-Y',$value['end_date'])."#form' title='Book Now on ".$value['yacht_name']." Galapagos Cruises - ".gmdate('F j',$value['start_date'])." at ".gmdate('F j - Y',$value['end_date'])."'>Book now!</a></td>";
                                        }else{
                                            $str .= '<td colspan="2">'.$value['comment'].'</td>';
                                        }    

                                        $str .= "</tr>";
                                        echo $str;
                                    }
                                }
                                echo '</table>';
                            }
			echo '<div class="theme-color txt-xl text-center"><a href="book-nemo-galapagos-cruises.php#form" class="btn btn-primary btn-lg" rel="nofollow" title="Contact us for more information Nemo '. $ng .' Galapagos Cruises"><i class="fa fa-info-circle" aria-hidden="true"></i> Contact us for more information</a></div>';
}
		
?>
