<?php
define("ROOT_PATH",$_SERVER["DOCUMENT_ROOT"] . "/");
define("BASE_URL","/");

function getNemosItinerary($itinerary) {
	require (ROOT_PATH . "inc/database.php");
	try{

		$strSQL = "SELECT itineraries.id, yatchs.nameyatch, itineraries.nameItinerary, itineraries.detailItinerary";
		$strSQL = $strSQL ." FROM yatchs INNER JOIN itineraries ON yatchs.id = itineraries.idYatch";
		$strSQL = $strSQL . " WHERE itineraries.id = ?";
		$results = $db->prepare($strSQL);
		$results->bindParam(1,$itinerary);
		$results->execute();

		$nemosItinerary = $results->fetchAll(PDO::FETCH_ASSOC);

	} catch(Exception $e){
		echo "Data could not be retrieved from the database.";
		exit;
	}
	return $nemosItinerary;


} 	


function getHeaderYatch($itinerary) {

	require (ROOT_PATH . "inc/database.php");

	try{

		$strSQL = "SELECT itineraries.id, cabins.namecabin, cabins.idyatch, cabins.idtype, cabins.order, cabins.linkpopup, types.nametype";
		$strSQL = $strSQL . " FROM itineraries INNER JOIN (cabins INNER JOIN types ON cabins.idtype = types.id) "; 
		$strSQL = $strSQL . " ON itineraries.idYatch = cabins.idyatch";
		$strSQL = $strSQL . " WHERE itineraries.id = ?";
		$strSQL = $strSQL ."  ORDER BY cabins.order ";

		$results = $db->prepare($strSQL);
		$results->bindParam(1,$itinerary);
		$results->execute();

		$headerYatch = $results->fetchAll(PDO::FETCH_ASSOC);

	} catch(Exception $e){
		echo "Data could not be retrieved from the database.";
		exit;
	}
	return $headerYatch;
}

function getHeaderYatchs($yatch) {

	require (ROOT_PATH . "inc/database.php");

	try{

		$strSQL = "SELECT cabins.namecabin, cabins.idyatch, cabins.idtype, cabins.order, cabins.linkpopup,types.nametype ";
		$strSQL = $strSQL . " FROM cabins INNER JOIN types ON cabins.idtype = types.id";
		$strSQL = $strSQL . " WHERE cabins.idyatch = ?";
		$strSQL = $strSQL . "  ORDER BY cabins.order ";

		$results = $db->prepare($strSQL);
		$results->bindParam(1,$yatch);
		$results->execute();

		$headerYatch = $results->fetchAll(PDO::FETCH_ASSOC);

	} catch(Exception $e){
		echo "Data could not be retrieved from the database.";
		exit;
	}
	return $headerYatch;
}


function getAvailabilitiesSearch($anio,$mes,$yatch) {

	require (ROOT_PATH . "inc/database.php");

	try{

		$strSQL = "SELECT * FROM  listAvailabilities ";
		$strSQL = $strSQL ."  WHERE YEAR(dateStart) = ? ";
		$strSQL = $strSQL ."  AND MONTH(dateStart) = ? ";
		if ($yatch > 0) $strSQL = $strSQL ."  AND idYatch = ? ";
		$strSQL = $strSQL ." ORDER BY idYatch,dateStart,idItineraryDate,idCabin ";
		
		$results = $db->prepare($strSQL);
		$results->bindParam(1,$anio);
		$results->bindParam(2,$mes);
		if ($yatch > 0) $results->bindParam(3,$yatch);

		$results->execute();

		$availabilities = $results->fetchAll(PDO::FETCH_ASSOC);
		$results->closeCursor();

	} catch(Exception $e){
		echo "Data could not be retrieved from the database.";
		exit;
	}

	return $availabilities;

}

function getAvailabilitiesItinerary($itinerary, $anio) {

	require (ROOT_PATH . "inc/database.php");

	try{
		$strSQL = "SELECT * FROM  listAvailabilities ";
		$strSQL = $strSQL ."  WHERE idItinerary = ? ";
		$strSQL = $strSQL ."  AND YEAR(dateStart) = ? ";
		$strSQL = $strSQL ."  ORDER BY dateStart, idCabin ";
		
		$results = $db->prepare($strSQL);
		$results->bindParam(1,$itinerary);
		$results->bindParam(2,$anio);
		$results->execute();

		$availabilities = $results->fetchAll(PDO::FETCH_ASSOC);
		$results->closeCursor();


	} catch(Exception $e){
		echo "Data could not be retrieved from the database.";
		exit;
	}

	return $availabilities;

}

function buildTableItinerary($itinerary, $anio) 
{


	$arrValues = getAvailabilitiesItinerary($itinerary,$anio);
	$arrHeader = getHeaderYatch($itinerary);


	$html ="";
	//tabla de leyendas	
    $html = $html . "<table style=\"width:100%;margin-bottom:15px;\">\n";
	$html = $html . "<tr>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available2-btn\">2 SPACES AVAILABLES</span></td>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available1-btn\">1 SPACE AVAILABLE</span></td>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available0-btn\">NOT AVAILABLE</span></td>\n";
 	$html = $html . "</tr>\n";
    $html = $html . "</table>\n";

	$html = $html . "<table class=\"table\">\n";

	//Obtener Cabecera Yatchs
	$html = $html . "<tr style=\"font-weight:bold;\">\n";
	$html = $html . "<td>Start</td>\n";
	$html = $html . "<td>End</td>\n";
	foreach ($arrHeader as $row){
		 
		 $html = $html . "<td>";
		 $html = $html . "<a href=\"#\" data-toggle=\"modal\" data-target=\"#".$row['linkpopup']."\">";
		 $html = $html . $row['namecabin']."<br>". $row['nametype'];
		 $html = $html . "</a></td>\n";
	}
	$html = $html . "<td style=\"text-align:center;\">Available<br><span style=\"font-size:12px;\">#passangers</span></td>\n";
	$html = $html . "</tr>\n";

	$title = true;
	$change = false;
	$currentDate= 0;
	$totalPax = 0;
	foreach ($arrValues as $row){

		if ($currentDate != $row['idItineraryDate']){
			if ($change) {
				$html = $html . "<td style=\"text-align:center;\">";
				if ($totalPax > 0 ){
					$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises"." - ". date("d F Y",strtotime($row['dateStart'])) 
					." at ". date("d F Y",strtotime($row['dateEnd'])) ."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
					.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&passangers=".$totalPax."\">";
				}
				$html = $html . "<span class=\"availablet-btn\">". $totalPax . "</span>";
				if ($totalPax > 0 ) $html = $html . "</a>";
				$html = $html . "</td>\n";
				$html = $html . "</tr>\n";
			}
			$totalPax = 0;
			$html = $html .  "<tr>\n";
			$html = $html . "<td>". date("d-M-Y",strtotime($row['dateStart'])) . "</td>\n" ;		
			$html = $html . "<td>". date("d-M-Y",strtotime($row['dateEnd'])) . "</td>\n" ;		
		}
		$html = $html . "<td>";		
		if ($row['passangers']>0){
			$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises ".$row['namecabin']." - ".date("d F Y",strtotime($row['dateStart']))." at " 
			.date("d F Y",strtotime($row['dateEnd']))."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
			.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&cabin=".$row['namecabin']."&passangers=".$row['passangers']."\">";
		}	
		
		$html = $html . "<span class=\"available". $row['passangers'] . "-btn\"></span>"; 
		
		$totalPax = $totalPax + $row['passangers'];

		if ($row['passangers']>0){
			$html = $html . "</a>";
		}	

		$html = $html . "</td>\n" ;

		$currentDate = $row['idItineraryDate'];
		$change = true;
	}
	$html = $html . "<td style=\"text-align:center;\">";
	if ($totalPax > 0 ){
		$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises "." - ". $row['dateStart'] 
		. " at " . $row['dateEnd'] ."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
		.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&passangers=".$totalPax."\">";
	}
	$html = $html . "<span class=\"availablet-btn\">". $totalPax . "</span>";
	if ($totalPax > 0 ) $html = $html . "</a>";
	$html = $html . "</td>\n";
	$html = $html . "</tr>\n";
	$html = $html . "</table>\n";
	// close the table

	//tabla de leyendas	
    $html = $html . "<table style=\"width:100%;margin-bottom:20px;\">\n";
	$html = $html . "<tr>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available2-btn\">2 SPACES AVAILABLES</span></td>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available1-btn\">1 SPACE AVAILABLE</span></td>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available0-btn\">NOT AVAILABLE</span></td>\n";
 	$html = $html . "</tr>\n";
    $html = $html . "</table>\n";

	return $html;
}


function buildTableAvailabilitiesSearch($anio,$mes,$yatch) 
{


	$arrValues = getAvailabilitiesSearch($anio,$mes,$yatch);
	


	$html ="";

	if (empty($arrValues)) return $html;


	//tabla de leyendas	
    $html = $html . "<table style=\"width:100%;margin-bottom:15px;\">\n";
	$html = $html . "<tr>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available2-btn\">2 SPACES AVAILABLES</span></td>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available1-btn\">1 SPACE AVAILABLE</span></td>\n";
	$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available0-btn\">NOT AVAILABLE</span></td>\n";
 	$html = $html . "</tr>\n";
    $html = $html . "</table>\n";



	$title = true;
	$change = false;
	$currentDate= 0;
	$totalPax = 0;

	$printHeaderBoat = true;
	$printFooterBoat = false;
	$currentYatch = 0;
	
	$numRows = 0;

	foreach ($arrValues as $row){
		
		$numRows = $numRows + 1;

		$yatch = $row['idyatch'];

		if ($currentYatch != $yatch ){
			if ($printFooterBoat){
				$html = $html . "<td style=\"text-align:center;\">";
				if ($totalPax > 0 ){
					$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises "." - ". $row['dateStart'] 
					. " at " . $row['dateEnd'] ."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
					.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&passangers=".$totalPax."\">";
				}
				$html = $html . "<span class=\"availablet-btn\">". $totalPax . "</span>";
				if ($totalPax > 0 ) $html = $html . "</a>";
				$html = $html . "</td>\n";
				$html = $html . "</tr>\n";
				$html = $html . "</table>\n";

				$printHeaderBoat = true;
				$title = true;
				$change = false;
				$currentDate= 0;
				$totalPax = 0;
			}

			if ($printHeaderBoat){
				$html=$html."<h3 style=\"margin:0;padding:10px 10px 0 10px;font-weight:bold;\">".$row['nameyatch']."</h3>";
				$html = $html . "<table class=\"table\">\n";

				//Obtener Cabecera Yatchs
				$html = $html . "<tr style=\"font-weight:bold;\">\n";
				$html = $html . "<td>Start</td>\n";
				$html = $html . "<td>End</td>\n";
				$html = $html . "<td style=\"text-align:center;\">Itinerary</td>\n";
				$arrHeader = getHeaderYatchs($yatch);
				foreach ($arrHeader as $row1){
					 $html = $html . "<td class=\"open-itinerary\">";
					 $html = $html . "<a href=\"#\" data-toggle=\"modal\" data-target=\"#".$row1['linkpopup']."\">";
					 $html = $html . $row1['namecabin']."<br>". $row1['nametype'];
					 $html = $html . "</a></td>\n";
				}
				$html = $html . "<td style=\"text-align:center;\">Available<br><span style=\"font-size:12px;\">#passangers</span></td>\n";
				$html = $html . "</tr>\n";

				$printHeaderBoat = false;			
				$printFooterBoat = true;
			}
		}



		if ($currentDate != $row['idItineraryDate']){
			if ($change) {
				$html = $html . "<td style=\"text-align:center;\">";
				if ($totalPax > 0 ){
					$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises"." - ". date("d F Y",strtotime($row['dateStart'])) 
					." at ". date("d F Y",strtotime($row['dateEnd'])) ."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
					.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&passangers=".$totalPax."\">";
				}
				$html = $html . "<span class=\"availablet-btn\">". $totalPax . "</span>";
				if ($totalPax > 0 ) $html = $html . "</a>";
				$html = $html . "</td>\n";
				$html = $html . "</tr>\n";
			}
			$totalPax = 0;
			$html = $html .  "<tr>\n";
			$html = $html . "<td>". date("d-M-Y",strtotime($row['dateStart']))."<span style=\"font-size:10px;\">(".date("D",strtotime($row['dateStart'])).")</span>"."</td>\n";
			$html = $html . "<td>". date("d-M-Y",strtotime($row['dateEnd'])) ."<span style=\"font-size:10px;\">(". date("D",strtotime($row['dateEnd'])).")</span>"."</td>\n" ;
			$html = $html . "<td style=\"text-align:center;\" class=\"open-itinerary\"><a rel=\"nofollow\" data-toggle=\"modal\" data-target=\"#nemositinerary\" href=\"nemos-itinerary.html?idItinerary="
			.$row['idItinerary']."\" title=\"".$row['nameItinerary']."\">". $row['shortItinerary']."</a></td>\n" ;			

		}
		$html = $html . "<td>";		
		if ($row['passangers']>0){
			$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises ".$row['namecabin']." - ".date("d F Y",strtotime($row['dateStart']))." at " 
			.date("d F Y",strtotime($row['dateEnd']))."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
			.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&cabin=".$row['namecabin']."&passangers=".$row['passangers']."\">";
		}	
		
		$html = $html . "<span class=\"available". $row['passangers'] . "-btn\"></span>"; 
		
		$totalPax = $totalPax + $row['passangers'];

		if ($row['passangers']>0){
			$html = $html . "</a>";
		}	

		$html = $html . "</td>\n" ;

		$currentDate = $row['idItineraryDate'];
		$currentYatch = $row['idyatch'];

		$change = true;
	}

	$html = $html . "<td style=\"text-align:center;\">";
	if ($totalPax > 0 ){
		$html = $html . "<a rel=\"nofollow\" title=\"Book Now on ". $row['nameyatch'] ." Galapagos Cruises "." - ". $row['dateStart'] 
		. " at " . $row['dateEnd'] ."\" href=\"nemobooking.html?arrival=".date("d-m-Y",strtotime($row['dateStart']))."&departure="
		.date("d-m-Y",strtotime($row['dateEnd']))."&boat=".$row['nameyatch']."&passangers=".$totalPax."\">";
	}
	$html = $html . "<span class=\"availablet-btn\">". $totalPax . "</span>";
	if ($totalPax > 0 ) $html = $html . "</a>";
	$html = $html . "</td>\n";
	$html = $html . "</tr>\n";
	$html = $html . "</table>\n";

	// close the table
	if ($numRows > 40){
		//tabla de leyendas	
	    $html = $html . "<table style=\"width:100%;margin-bottom:20px;\">\n";
		$html = $html . "<tr>\n";
		$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available2-btn\">2 SPACES AVAILABLES</span></td>\n";
		$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available1-btn\">1 SPACE AVAILABLE</span></td>\n";
		$html = $html . "<td style=\"width:33%;text-align:center;\"><span class=\"available0-btn\">NOT AVAILABLE</span></td>\n";
	 	$html = $html . "</tr>\n";
	    $html = $html . "</table>\n";
	}
	    

	return $html;
}