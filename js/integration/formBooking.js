var messageDelay = 2000;
$(function(){ 

	var emailreg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;	
	$("#sendButton").click(function(){  

		var formBooking = $('#formBooking');
		
		$(".error").fadeOut().remove();
		
  	    var regex = /^[0-9\ ]+$/;
		var n1 = parseInt($('#n1').val());
		var n2 = parseInt($('#n2').val());
		var expect = n1 + n2;

		if ($("#name").val() == "") {  
			$("#name").focus().after('<span class="error">Enter your Name and Surname</span>');  
			return false;  
		}  
		if ($("#email").val() == "" || !emailreg.test($("#email").val())) {
			$("#email").focus().after('<span class="error">Enter your Email</span>');  
			return false;  
		}  
		if ($("#country").val() == "" ) {
			$("#country").focus().after('<span class="error">Select an option</span>');  
			return false;  
		}  
        if ($("#hear").val() == "" ) {
			$("#hear").focus().after('<span class="error">Select an option</span>');  
			return false;  
		}  
		if ($("#message").val() == "") {  
			$("#message").focus().after('<span class="error">Enter your message</span>');  
			return false;  
		}
		if ( !$('#expect').val() || !regex.test($('#expect').val())){
			$("#expect").focus().after('<span class="error">Invalid input</span>');  
			return false;  
		}else if (parseInt($('#expect').val()) != expect) {
			$("#expect").focus().after('<span class="error">' + n1 + ' + ' + n2 + ' = Wrong answer </span>');  
			return false;  					
		}

		$('#sendingMessage').fadeIn();
			$.ajax( {
			url: formBooking.attr( 'action' ) + "?ajax=true",
			type: formBooking.attr( 'method' ),
			data: formBooking.serialize(),
			success: submitFinished
		});
		// Prevent the default form submission occurring
		return false;
	});  
			
	$("#name, #country, #hear, #message , #expect ").bind('blur keyup', function(){  
    	if ($(this).val() != "") {  			
			$('.error').fadeOut();
			return false;  
		}  
	});	
			
	$("#email").bind('blur keyup', function(){  
    	if ($("#email").val() != "" && emailreg.test($("#email").val())) {	
			$('.error').fadeOut();  
			return false;  
		}  
	});
});

function submitFinished( response ) {
	response = $.trim( response );
	$('#sendingMessage').fadeOut();
	if ( response == "success" ) {
    	$('#successMessage').fadeIn().delay(messageDelay).fadeOut();
	    $('#name').val( "" );
    	$('#email').val( "" );	
	    $('#passangers').val( "" );	
	    $('#hear').val( "" );		
		$('#country').val( "" );
	    $('#arrivalDate').val( "" );
		$('#departureDate').val( "" );
		$('#message').val( "" );
	    $('#expect').val( "" );
    	window.location = 'nemo-thank-contacting.html';

	} else {
    	$('#failureMessage').fadeIn().delay(messageDelay).fadeOut();
	}
}