angular.module('AvailableApp', [])

    .controller('mainController', function ($scope, $http, $window) {
        $scope.requestItinerary = {
            id: "",
            data: [],
            days: {},
            charter: "REQUEST CABINS",
            sumPax: 0,
            typeId: "NA",
            cruiseId: "",
            rest: 0,
            exceeded: "",
            extra: {start: "", end: "", itinerary: "", color: "", yatch: "", itinerary_pic: ""},
            form: {
                name: "",
                email: "",
                country: "",
                countryName: "",
                hear: "",
                payment: "",
                reference: "",
                reservationState: "Reservado",
                rate: "",
                amount: '0.00',
                charter: false
            },
            finish: {op: true, msj: ""}
        };
        $scope.originalCabins = [];
        $scope.requestCabins = {
            id: "",
            data: [],
            charter: "REQUEST CABINS",
            sumPax: 0,
            typeId: "NA",
            cruiseId: "",
            rest: 0,
            exceeded: "",
            extra: {start: "", end: "", itinerary: "", color: "", yatch: "", yatchCode: ""},
            form: {
                name: "",
                email: "",
                country: "",
                countryName: "",
                hear: "",
                payment: "",
                reference: "",
                reservationState: "Reservado",
                rate: "",
                amount: '0.00',
                charter: false
            },
            finish: {op: true, msj: "", class: "fa-check-circle"},
            warning: ""
        };
        $scope.cabin_image = "";
        $scope.form_image = "";
        $scope.interval_loading = false;
        $scope.cabinShow = function (op) {
            $scope.cabin_image = op;
        };
        $scope.formShow = function (op) {
            //console.log(op);
            $scope.form_image = op;
        };
        $scope.selectedYacht = {data: {}};
        $scope.yachts = [
            {
                "id": ["59c86f6f221a3b136a70c4e7"],
                "name": "Nemo II",
                "$$hashKey": "object:26"
            },
            {
                "id": ["59c86f6f221a3b136a70c4e8"],
                "name": "Nemo III",
                "$$hashKey": "object:27"
            },
        ];
        var allYachts = [];
        var tmp_all = {id: allYachts, name: "All Yachts"};
        $scope.yachts.push(tmp_all);
        $scope.selectedYacht.data = tmp_all;
        $scope.dateToAvailability = [];

        $scope.selectedFromDate = {data: ""};
        $scope.selectedToDate = {data: ""};


        lastDate = "2020-01-05T06:00:00.000Z";
        firstDate = new Date();
        var current = new Date(firstDate.getFullYear(), firstDate.getMonth(), 1);
        var diff_months = mydiff(firstDate, lastDate, 'months');

        for (i = 0; i < diff_months; i++) {
            current = new Date(current);
            var formatedDate = myYearAndMonth(current);
            if ($scope.selectedFromDate.data === "") {
                $scope.selectedFromDate.data = formatedDate.value;
                $scope.startDate = new Date($scope.selectedFromDate.data);
            }


            if ($scope.selectedToDate.data === "" && i === diff_months - 1) {
                $scope.selectedToDate.data = formatedDate.value;
                toDate = new Date($scope.selectedToDate.data);
                $scope.endDate = new Date(toDate.getFullYear(), toDate.getMonth() + 1, 0);
            }
            $scope.dateToAvailability.push(formatedDate);
            current = current.setMonth(current.getMonth() + 1);
        }

        $scope.setDateFilterAdvanceReport = function () {

            $scope.startDate = new Date($scope.selectedFromDate.data);
            $scope.endDate = new Date($scope.selectedToDate.data);
            $scope.endDate = new Date($scope.endDate.getFullYear(), $scope.endDate.getMonth() + 1, 1);

            $scope.startDate.setHours($scope.startDate.getHours() - 5);
            $scope.startDate.setMinutes(00);

            $scope.endDate.setHours($scope.endDate.getHours() - 5);
            $scope.endDate.setMinutes(00);

            $scope.startDate = $scope.startDate.getTime() / 1000;
            $scope.endDate = $scope.endDate.getTime() / 1000;
            $scope.data = [];
            var searchParams = JSON.parse(localStorage.getItem("searchNemos"));

            var yacht = "nemo2,nemo3";


            $scope.offerName = "EARLY BIRD";
            $scope.year = 2019;
            offer = "earlyBird";

            // var url = `https://availability.nemofleet.com/apiOffersGI.php?yacht=${searchParams.yacht || yacht}&itinerary=all&part=all&howMany=200&startDate=${searchParams.fromDate === -1 ? 1556672400 : searchParams.fromDate / 1000}&endDate=${searchParams.toDate === -1 ? 1577836800 : searchParams.toDate / 1000}&year=2019&offer=earlyBird`
            var url = `https://availability.nemofleet.com/apiOffersGI.php?yacht=${searchParams.yacht || yacht}&itinerary=all&part=all&howMany=200&startDate=${1556672400}&endDate=${1577836800}&year=2019&offer=earlyBird`

            $http.get(url)
                .success(function (data) {
                    console.log(data);
                    $scope.rows = data.data;
                })
                .error(function (err) {

                });


        }

        $scope.selectItinerary = function (startDate, endDate, itinerary, color, yatchName, pic) {
            $scope.requestItinerary.extra = {
                start: startDate,
                end: endDate,
                itinerary: itinerary,
                color: color,
                yatch: yatchName,
                itinerary_pic: pic
            };
            //console.log($scope.requestItinerary);
        };

        $scope.selectCabin = function (cabin, cabinsHowMany, cruiseId, type, maxCapacity, taken, startDate, endDate, itinerary, color, yatchName, yatchCode, originalCabins, index) {

            var foundit = false;
            //console.log($scope.requestCabins.data);
            angular.forEach($scope.requestCabins.data, function (value, key) {
                if (value.name === cabin.name) {
                    $scope.requestCabins.data.splice(key, 1);
                    $scope.requestCabins.sumPax = parseInt($scope.requestCabins.sumPax) - 1;
                    foundit = true;
                }
            });

            if (!foundit) {
                $scope.requestCabins.data.push(cabin);
                $scope.requestCabins.id = index;
                var sumPax = 0;
                angular.forEach($scope.requestCabins.data, function (value, key) {
                    sumPax = sumPax + parseInt(value.availiabilitySelected);
                    if (value.numberPax === 1 && value.availiability === value.availiabilityObj.length) {
                        if (value.availiability > 2) {
                            value.availiabilityObj = [];
                            value.availiabilityObj.push({key: 1, value: 1});
                        } else {
                            value.availiabilityObj.splice((value.availiabilityObj.length - 1), 1);
                        }
                    }
                });

                $scope.requestCabins.rest = parseInt(taken);
                $scope.requestCabins.sumPax = sumPax;
                $scope.requestCabins.typeId = type;
                $scope.requestCabins.cruiseId = cruiseId;
                $scope.requestCabins.extra = {
                    start: startDate,
                    end: endDate,
                    itinerary: itinerary,
                    color: color,
                    yatch: yatchName,
                    yatchCode: yatchCode
                };
            }

            if ($scope.requestCabins.data.length > 0) {
                angular.forEach($scope.rows, function (value, key) {
                    if (key !== index) {
                        angular.forEach(value.cabins, function (value2, key2) {
                            value2.disabledDynamic = true;
                        });
                    } else {
                        value.request = false;
                    }
                });
            } else {
                angular.forEach($scope.rows, function (value, key) {
                    value.request = true;
                    angular.forEach(value.cabins, function (value2, key2) {
                        value2.disabledDynamic = false;
                    });
                });
            }
            //Charter
            if (cabinsHowMany === $scope.requestCabins.data.length) {
                $scope.requestCabins.form.charter = true;
                $scope.requestCabins.buttonText = "REQUEST CHARTER";
            } else {
                $scope.requestCabins.form.charter = false;
                $scope.requestCabins.buttonText = "REQUEST CABINS";
            }
        };


        function mydiff(date1, date2, interval, noTime) {
            var second = 1000, minute = second * 60, hour = minute * 60, day = hour * 24, week = day * 7;
            if (noTime) {
                var tmp = new Date(date1);
                date1 = new Date(tmp.getYear(), tmp.getMonth(), tmp.getDate(), 00, 00, 00);
                var tmp = new Date(date2);
                date2 = new Date(tmp.getYear(), tmp.getMonth(), tmp.getDate(), 00, 00, 00);
            } else {
                date1 = new Date(date1);
                date2 = new Date(date2);
            }
            var timediff = date2 - date1;
            if (isNaN(timediff))
                return NaN;
            switch (interval) {
                case "years":
                    return date2.getFullYear() - date1.getFullYear();
                case "months":
                    return (
                        (date2.getFullYear() * 12 + date2.getMonth())
                        -
                        (date1.getFullYear() * 12 + date1.getMonth())
                    );
                case "weeks"  :
                    return Math.floor(timediff / week);
                case "days"   :
                    return Math.floor(timediff / day);
                case "hours"  :
                    return Math.floor(timediff / hour);
                case "minutes":
                    return Math.floor(timediff / minute);
                case "seconds":
                    return Math.floor(timediff / second);
                default:
                    return undefined;
            }
        };

        $scope.externalLink = function (url) {
            $window.open(url);
        };

        function myYearAndMonth(current) {
            var ans = {};
            var year = current.getFullYear();
            var month = current.getMonth() + 1;
            var month_tmp = current.getMonth();
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            if (month_tmp > (monthNames.length) - 1) {
                month_tmp = 0;
                year = year + 1;
            }
            if (parseInt(month) > 12) {
                month = 1;
            }
            if (parseInt(month) < 10) {
                month = '0' + month;
            }
            if (angular.isDefined(monthNames[month_tmp])) {
                ans = {title: year + ' - ' + monthNames[month_tmp], value: year + '-' + month + '-01T06:00:00Z'};
            } else {
                ans = {title: year + ' - ' + monthNames[0], value: year + '-' + month + '-01T06:00:00Z'};
            }
            return ans;
        }

        $scope.sumSelectedPax = function (cabin) {
            var sumit = 0;
            angular.forEach($scope.requestCabins.data, function (value, key) {
                sumit += parseInt(value.availiabilitySelected);
            });
            $scope.requestCabins.sumPax = sumit;

            if (cabin.availiabilitySelected > 1) {
                cabin.single = false;
            }
        };


        $scope.submitForm = function (isValid) {
            $scope.submitted = true;
            if ($scope.requestCabins.sumPax > $scope.requestCabins.rest) {
                $scope.requestCabins.exceeded = "Catamaran capacity exceeded.";
            } else {
                if (isValid) {

                    console.log($scope.requestCabins.form.country);

                    var cabins = "";
                    angular.forEach($scope.requestCabins.data, function (cabin) {
                        cabins += "Cabin No. " + cabin.name + " : " + cabin.availiabilitySelected + " pax" + " / "
                    });
                    var data = $.param({
                        yacht: $scope.requestCabins.extra.yatch,
                        from: $scope.requestCabins.extra.start,
                        until: $scope.requestCabins.extra.end,
                        itinerary: $scope.requestCabins.extra.itinerary,
                        cabins: cabins,
                        name: $scope.requestCabins.form.name_a,
                        email: $scope.requestCabins.form.email_a,
                        country: $scope.requestCabins.form.country_a,
                        countryName: $scope.requestCabins.form.countryName_a,
                        hear: $scope.requestCabins.form.hear_a,
                        payment: $scope.requestCabins.form.payment_a

                    });

                    var config = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                        }
                    }
                    console.log(data);

                    // $http.post('availabilityForm.php', data, config)
                    // .success(function (data, status, headers, config) {
                    //     $scope.PostDataResponse = data;
                    //     if ($scope.PostDataResponse = "success"){
                    //         $scope.requestCabins.finish.op = true;
                    //         $scope.requestCabins.finish.msj = "The requirement has been processed successfully";
                    //         $scope.requestCabins.finish.class = "fa-check-circle";
                    //         $scope.requestCabins.data = [];
                    //         $scope.requestCabins.sumPax = 0;
                    //         $scope.requestCabins.typeId = "NA";
                    //         $scope.requestCabins.cruiseId = "";
                    //         $scope.requestCabins.rest = 0;
                    //         $scope.requestCabins.exceeded = "";
                    //         $scope.requestCabins.form.name_a = "";
                    //         $scope.requestCabins.form.email_a = "";
                    //         $scope.requestCabins.form.country_a = "";
                    //         $scope.requestCabins.form.countryName_a = "";
                    //         $scope.requestCabins.form.hear_a = "";
                    //         $scope.requestCabins.form.payment_a = "";
                    //
                    //         $scope.setDateFilterAdvanceReport();
                    //     }else {
                    //         $scope.requestCabins.finish.op = true;
                    //         $scope.requestCabins.finish.msj = "Sorry something went wrong, try again later";
                    //         $scope.requestCabins.finish.class = "fa-exclamation-triangle";
                    //     }
                    //
                    // })
                    // .error(function (data, status, header, config) {
                    //     $scope.ResponseDetails = "Data: " + data +
                    //         "<hr />status: " + status +
                    //         "<hr />headers: " + header +
                    //         "<hr />config: " + config;
                    //
                    //     $scope.requestCabins.finish.msj = $scope.PostDataResponse;
                    //     $scope.requestCabins.finish.class = "fa-exclamation-triangle";
                    // });
                }
            }
        }


    });
