$(document).ready(function(){
	$('#search').on('click',function(){
		var yatch = $('#yatch').val();
		var period = $('#period').val();
		var dataString = 'yatch='+ yatch + '&period=' + period;
		$("#contentSearch").empty();
		$('#contentSearch').append('<p style="text-align:center;"><img src="../img/AjaxLoader.gif"/><span style="padding-left:10px;">Searching...</span></p>');
		if (yatch == '' || period == '' ){
			alert('Please fill in Yatch and Period');
		}
		else{
			$.ajax({
				type:"POST",
				url:"../inc/nemos-availability.php",
				data:dataString, 
				cache:false,
				success:function(html){
					$("#contentSearch").empty();
					$('#contentSearch').append(html);
				}
			});
		}
		return false;
	});

	$('#yatch').on('change',function(){
		$("#contentSearch").empty();
	});
	$('#period').on('change',function(){
		$("#contentSearch").empty();
	});
});