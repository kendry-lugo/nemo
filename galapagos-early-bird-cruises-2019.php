<!DOCTYPE html>
<html lang="en">
<head>

    <!--Meta Codificación de caracteres-->
    <meta charset="utf-8">

    <!--Meta Compatibilidad-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--Meta Responsive-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Título del Sitio-->
    <title>Nemo Galapagos Islands Cruises | Tours in Galapagos Islands</title>

    <!--Meta Descripción del Sitio-->
    <meta name="description" content="NEMO GALAPAGOS CRUISES is a family business that has been sailing in the Galapagos
	Islands since 1985. We offer first class cruises to the Galapagos Islands. We own three boats available for cruises
	to the Galapagos Islands; Nemo I, Nemo II, Nemo III.  We offer Tours in Galapagos Island."/>

    <!--Meta Palabras Claves-->
    <meta name="keywords" content="Galapagos islands cruises, Nemo Galapagos islands cruises, Galapagos Islands Travel,
	Galapagos last minute cruises, Galapagos cruise deals, Galapagos Islands Tours."/>

    <!--Meta Autor-->
    <meta name="author" content="Latintour Cia. Ltda." />

    <!--Meta Publicación-->
    <meta name="publisher" content="Latintour Cia. Ltda." />

    <!--Meta Derechos Intelectuales del Sitio-->
    <meta name="copyright" content="Latintour Cia. Ltda." />

    <!--Meta Distribución-->
    <meta name=”distribution” content=”global”/>

    <!--Meta Robots-->
    <meta name="robots" content="index, follow" />

    <!--Meta Canonical (Evita Duplicado)-->
    <link rel=”canonical” href=”https://nemogalapagoscruises.com/” />

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="76x76" href="img/logos/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/logos/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/logos/favicon/favicon-16x16.png">
    <!--<link rel="manifest" href="img/logos/favicon/site.webmanifest">-->
    <link rel="mask-icon" href="img/logos/favicon/safari-pinned-tab.svg" color="#0c345c">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- link to font awesome -->
    <link media="all" rel="stylesheet" href="vendors/font-awesome/css/font-awesome.css">
    <!-- link to custom icomoon fonts -->
    <link rel="stylesheet" type="text/css" href="css/fonts/icomoon/icomoon.css">
    <!-- link to wow js animation -->
    <link media="all" rel="stylesheet" href="vendors/animate/animate.css">
    <!-- include bootstrap css -->
    <link media="all" rel="stylesheet" href="css/bootstrap.css">
    <!-- include owl css -->
    <link media="all" rel="stylesheet" href="vendors/owl-carousel/owl.carousel.css">
    <link media="all" rel="stylesheet" href="vendors/owl-carousel/owl.theme.css">
    <!-- link to revolution css  -->
    <link rel="stylesheet" type="text/css" href="vendors/revolution/css/settings.css">
    <!-- include main css -->
    <link media="all" rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="preloader" id="pageLoad">

    <div class="holder">
        <!--<div class="coffee_cup"></div> -->
    </div>
</div>


<!-- main wrapper -->
<div id="wrapper">
    <div class="page-wrapper">
        <!-- main header -->
        <header id="header">
            <div class="container-fluid">
                <!-- logo -->
                <div class="logo">
                    <a href="index.html">
                        <img class="normal" src="img/logos/logo-latin-tour.png" alt="Logo Nemo Galapagos Cruises">
                    </a>
                </div>
                <!-- main navigation -->
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle nav-opener" data-toggle="collapse" data-target="#nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- main menu items and drop for mobile -->
                    <div class="collapse navbar-collapse" id="nav">
                        <!-- main navbar -->
                        <ul class="nav navbar-nav">
                            <!--INICIO-->
                            <li class="dropdown">
                                <a href="index.html" class="dropdown-toggle" data-toggle="dropdown">Home <b class="icon-angle-down"></b></a>
                            </li>
                            <!--FLOTA NEMO-->
                            <li class="dropdown has-mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">NEMO FLEET <b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu">
                                    <div class="drop-wrap">
                                        <div class="drop-holder">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="col">
                                                        <div class="img-wrap">
                                                            <a href="nemo-I.html"><img src="img/generic/nemo1.jpg" height="175" width="350" alt="image description"></a>
                                                        </div>
                                                        <div class="des">
                                                            <strong class="title"><a href="nemo-I.html">NEMO I</a></strong>
                                                            <p>With a length of 24.90 meters and a width of 10 meters, this Catamaran reaches 10 knots and accommodates up to 14 guests in comfortable cabins.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="col">
                                                        <div class="img-wrap">
                                                            <a href="nemo-II.php"><img src="img/generic/nemo2.jpg" height="175" width="370" alt="image description"></a>
                                                        </div>
                                                        <div class="des">
                                                            <strong class="title"><a href="nemo-II.php">NEMO II</a></strong>
                                                            <p>With a length of 21.88 meters and a width of 10.39 meters, this Catamaran reaches 10 knots and accommodates up to 14 guests in comfortable cabins.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="col">
                                                        <div class="img-wrap">
                                                            <a href="nemo-III.php"><img src="img/generic/nemo3.jpg" height="175" width="370" alt="image description"></a>
                                                        </div>
                                                        <div class="des">
                                                            <strong class="title"><a href="nemo-III.php">NEMO III</a></strong>
                                                            <p>With a length of 23 meters and a width of 11.40 meters, this Catamaran reaches 10 knots and accommodates up to 16 guests in comfortable cabins.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--VISITA LA ISLA-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Island Hopping <b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu mt-0">
                                    <ul>
                                        <li><a href="galapagos-island-4-days.html">GALAPAGOS ISLAND HOPPING 4 DAYS</a></li>
                                        <li><a href="galapagos-inter-islands-5-days.html">GALAPAGOS ISLAND HOPPING 5 DAYS</a></li>
                                    </ul>
                                </div>
                            </li>
                            <!--TOURS-->
                            <li class="dropdown has-mega-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mainland Tours<b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu">
                                    <div class="drop-wrap">
                                        <div class="five-col">
                                            <div class="column">
                                                <strong class="title sub-link-opener">Ecuador - Andes</strong>
                                                <ul class="header-link">
                                                    <li><a href="cotopaxi.html">COTOPAXI</a></li>
                                                    <li><a href="ecuador-by-train.html">ECUADOR BY TRAIN</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">Ecuador - Amazon</strong>
                                                <ul class="header-link">
                                                    <li><a href="napo-wildlife.html">NAPO WILDLIFE CENTER</a></li>
                                                    <li><a href="cuyabeno-lodge.html">CUYABENO LODGE</a></li>
                                                    <li><a href="sacha-lodge.html">SACHA LODGE</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">Ecuador - Day Trips</strong>
                                                <ul class="header-link">
                                                    <li><a href="indigenous-market.html">INDIGENOUS MARKETS</a></li>
                                                    <li><a href="quito-city.html">QUITO CITY TOUR AND MIDDLE OF THE WORLD</a></li>
                                                    <li><a href="mindo.html">MINDO CLOUD FOREST</a></li>
                                                    <li><a href="papallacta-thermas.html">PAPALLACTA THERMAS & ROSE FARM</a></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">Perú</strong>
                                                <ul class="header-link">
                                                    <li><a href="machupicchu-6days.html">LIMA-CUSCO-MACHUPICCHU</a></li>
                                                    <li><a href="machupicchu-5days.html">CUSCO - MACHUPICCHU 5 DAYS</a></li>
                                                    <li><a href="machupicchu-4days.html">CUSCO - MACHUPICCHU 4 DAYS</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>
                                            <div class="column">
                                                <strong class="title sub-link-opener">BOLÍVIA</strong>
                                                <ul class="header-link">
                                                    <li><a href="bolivia-4days.html">BOLIVIA AT GLANCE 4 DAYS</a></li>
                                                    <li><a href="bolivia-5days.html">BOLIVIA COMFORTABLE 5 DAYS</a></li>
                                                    <li><a href="bolivia-6days.html">BOLIVIA MAJESTIC 6 DAYS</a></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                    <li></li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!--OFFER-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Special Offers <b class="icon-angle-down"></b></a>
                                <div class="dropdown-menu mt-0">
                                    <ul>
                                        <li><a href="#">PACKAGE GALAPAGOS + PAPALLACTA HOT SPRINGS</a></li>
                                        <li><a href="#">EARLY BIRD GALAPAGOS CRUISES 2019</a></li>
                                    </ul>
                                </div>
                            </li>
                            <!--ABOUT US-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="icon-angle-down"></b></a>
                            </li>
                            <!--CONTACT US-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact Us  <b class="icon-angle-down"></b></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

        </header>

			<!-- main container -->
			<main id="main">
				<!-- main tour information -->
				<section class="container-fluid trip-info">
					<div class="same-height row">
						<div class="height col-md-12">
							<!-- top image slideshow -->
							<div id="tour-slide">
								<div class="slide">
									<div class="bg-stretch">
										<img src="img/nemoII/slider/nemo2-1.jpg" title="Nemo II" alt="Nemo II" height="933" width="1400">
									</div>
								</div>
								<div class="slide">
									<div class="bg-stretch">
										<img src="img/nemoII/slider/nemo2-2.jpg" title="Nemo II" alt="Nemo II" height="933" width="1400">
									</div>
								</div>
								<div class="slide">
									<div class="bg-stretch">
										<img src="img/nemoII/slider/nemo2-3.jpg" title="Nemo II" alt="Nemo II" height="933" width="1400">
									</div>
								</div>
							</div>
						</div>

					</div>
				</section>
				<div class="tab-container">
					<nav class="nav-wrap" id="sticky-tab">
						<div class="container">
							<!-- nav tabs -->
							<ul class="nav nav-tabs text-center" role="tablist">
								<li role="presentation" class="active"><a href="#tab01" aria-controls="tab01" role="tab" data-toggle="tab">Overview</a></li>
								<li role="presentation"><a href="#tab02" aria-controls="tab02" role="tab" data-toggle="tab">Thechnical Specifications</a></li>
								<li role="presentation"><a href="#tab03" aria-controls="tab03" role="tab" data-toggle="tab">Accommodation</a></li>
								<li role="presentation"><a href="#tab04" aria-controls="tab04" role="tab" data-toggle="tab">Availability & Itinerary</a></li>
							</ul>
						</div>
					</nav>
					<!-- tab panes -->
					<div class="container tab-content trip-detail">
						<!-- overview tab content -->
						<div role="tabpanel" class="tab-pane active" id="tab01">
							<div class="row">
								<div class="col-xs-12">
									<div class="detail">
                                        <h3 class="header-box">NEMO II GALAPAGOS ISLANDS CRUISE</h3>
                                        <p>What would be more beautiful than taking a cruise on the mystical Galapagos
										   Islands, the NEMO GALAPAGOS CRUISES offer this and much more. All NEMO
										   GALAPAGOS CRUISES provide epic vacations and years of experience in
										   stellar service. NEMO GALAPAGOS CRUISES has been a leader in the industry
										   for decades and continues to be at the forefront. The NEMO II GALAPAGOS
										   CRUISES offers an all-inclusive 8-day cruise to its passengers with daily
										   excursions when traveling to the Galapagos Islands. This is a service of
										   the NEMO II GALAPAGOS CRUISES with guided tours and all meals included.
										</p>

                                        <h3 class="header-box">NEMO II GALAPAGOS ISLANDS CRUISE SERVICES:</h3>
                                        <p class="mb-10">
											This comprehensive vacation in NEMO II GALAPAGOS CRUISES allows you to stay
											in double cabins, each with its own private bathroom. Delicious meals will
											be provided three times a day with local menus created by the most talented
											chefs in the country, who will take their vacations as a culinary adventure.
											You will be accompanied by a certified bilingual guide while visiting each
											island and will give you a detailed historical tour with sandwiches when you
											return to the NEMO II GALAPAGOS CRUISES. Back aboard the NEMO II GALAPAGOS
											CRUISES, you can enjoy showers of hot or cold water, depending on what you
											prefer, since it is a very hot climate. All NEMO GALAPAGOS CRUISES will have
											air conditioning and towels will be provided to your liking. NEMO II
											GALAPAGOS CRUISES has an excellent captain and crew very experienced and
											attentive to the needs of its passengers. Our goal is to make sure you have
											a complete vacation in NEMO II GALAPAGOS CRUISES. The climate is divine
											throughout the year, we have an experienced crew, several daily excursions,
											what else can you ask for on the Galapagos Islands cruises.
                                        </p>
                                        <ul class="content-list tick-list">
                                            <li>NEMO Galapagos cruises provide three meals a day and snacks after each
                                                excursion.
                                            </li>
                                            <li>There is an experienced bilingual naturalist guide that provides an
                                                entertaining discourse on the history on every NEMO Galapagos cruise.
                                            </li>
                                            <li>On NEMO Galapagos cruises, depending on the number of people in
                                                attendance, you can select the cabin you prefer on the.
                                            </li>
                                            <li>On Nemo Galapagos cruises, each cabin is updated with a shower and en
                                                suite facilities.</li>
                                            <li>For couples, you have a double bed in a double cabin or there are also
                                                cabins with a double bed with an additional upper bunk above.</li>
                                        </ul>
                                        <p class="mt-20">The most visited islands that are part of the itinerary of NEMO
											II GALAPAGOS CRUISES include Santa Cruz, North Seymour, Floreana and Baltra.
											Traveling to the Galapagos Islands is having a unique experience in life.
											The NEMO II GALAPAGOS CRUISES travels through these amazing and historic
											islands. In your NEMO II GALAPAGOS CRUISES through the Galapagos Islands,
											you will disembark every day to set foot on the magic terrain and swim with
											the exotic animals. Once you return home after your incredible vacation in
											NEMO II GALAPAGOS CRUISES, you will fantasize and be amazed with your
											friends about how incredible this trip was and, hopefully, you will
											influence others to take a look at the NEMO GALAPAGOS CRUISES and You will
											begin to dream of your own holidays in NEMO II GALAPAGOS CRUISES.
										</p>
										<div class="row">
											<div class="col-sm-4">
												<img src="img/nemoI/images/NEMO_I-2.jpg" title="Nemo I Galapago Cruises" alt="Nemo I Galapago Cruises">
											</div>
											<div class="col-sm-4">
												<img src="img/nemoI/images/NEMO_I-9.jpg" title="Nemo I Galapago Cruises" alt="Nemo I Galapago Cruises">
											</div>
											<div class="col-sm-4">
												<img src="img/nemoI/images/NEMO_I-24.jpg" title="Nemo I Galapago Cruises" alt="Nemo I Galapago Cruises">
											</div>
										</div>
										<p class="mt-50">
											NEMO II GALAPAGOS CRUISES places special emphasis on each cruise to ensure
											that your trip goes smoothly. There is an impeccable team in the NEMO II
											GALAPAGOS CRUISES, they are extremely professional with years of service
											dedicated to the NEMO Galapagos cruises. Beyond the crew, each passenger has
											its own private suite to relax and reflect on the day's activities aboard
											the NEMO II GALAPAGOS CRUISES when traveling to the Galapagos Islands.
										</p>
										<p>
                                            Every day in your NEMO II GALAPAGOS CRUISES are full of various activities
                                            to ensure a fun-filled day. This cruise on the island of Galápagos is eight
                                            days and seven nights with a daily program to ensure you enjoy the
                                            experience offered by the cruises of NEMO II GALAPAGOS CRUISES which is a
                                            leading competitor in the industry, we take pride in our products. The NEMO
                                            II GALAPAGOS CRUISES has historically been an excellent and complete cruise
                                            so it is still very popular among all the NEMO GALAPAGOS CRUISES.
										</p>
	                                    <h3 class="header-box">UNFORGETABLE EXPERIENCE:</h3>
                                        <p>
                                            Experiencing the islands is a privilege in itself, since it is only known by
                                            a few and being able to have it aboard a NEMO II GALAPAGOS CRUISES makes the
                                            experience more special. On this NEMO II GALAPAGOS CRUISES vacation, you
                                            will find some of the most exotic species of flora and fauna in the world.
                                            Do not miss this exciting opportunity to travel to the Galapagos Islands
                                            that truly are a paradise on earth.
                                        </p>

									</div>
								</div>
							</div>
						</div>
                        <!-- Thechnical Specifications tab content -->
                        <div role="tabpanel" class="tab-pane" id="tab02">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="detail">
                                        <h3 class="header-box">NEMO II GALAPAGOS CRUISE THECHNICAL SPECIFICATIONS</h3>
                                        <p>
                                            When going on your NEMO II Galapagos cruise you want to know all the details
                                            so you can make an accurate decision on your vacation and the NEMO II
                                            Galapagos islands cruise specification provides that insurance. The NEMO II
                                            Galapagos islands cruise specifications provides more insight into
                                            characteristics, technical equipment, accommodations, security and the deck
                                            plan. In the characteristics tab of the NEMO II Galapagos islands cruise
                                            specification provides the stats of the vessel, the capacity up to 14
                                            passengers, and how many members the crew entails which is different on each
                                            of the NEMO Galapagos cruises. The NEMO II Galapagos cruise specifications
                                            then goes onto the technical equipment and the additional items that make
                                            for a luxury vacation, which is the same on every NEMO Galapagos Cruise. The
                                            following section of the NEMO II Galapagos cruise specifications is the
                                            security portion to put the passenger’s minds at ease that if there was any
                                            unexpected event we would be prepared.
                                        </p>
                                        <p>
                                            All NEMO Galapagos cruises are up to
                                            code with safety regulations and our crew is well trained for any sort of
                                            event that may occur, which is displayed in this portion of the NEMO II
                                            Galapagos islands cruise specifications. Afterwards, the NEMO II Galapagos
                                            islands cruse specification gives a brief description of the accommodations
                                            but each of the NEMO Galapagos cruises goes into much more detail in the
                                            Accommodations section. The final tab of the NEMO II Galapagos cruise
                                            specifications is the deck plan where you can have the technical viewpoint
                                            of what your vessel looks like. We provide this for every NEMO Galapagos
                                            islands cruises so passengers can visualize and see where the emergency
                                            exits are located if the need were to arise. If there are any additional
                                            items that we have not highlighted in the NEMO II Galapagos islands cruise
                                            specifications sections please do not hesitate to ask and we can assure that
                                            the NEMO II Galapagos cruise is the right choice for you.
                                        </p>

                                    </div>
                                </div>
                                <div class="col-md-6 accomodation-block">
                                    <p>
                                        Below you will find the details of your NEMO II GALAPAGOS CRUISE specifications.
                                    </p>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse1">CHARACTERISTICS</a>
												</h4>
											</div>
											<div id="collapse1" class="panel-collapse collapse">
												<div class="panel-body">
													<ul class="content-list tick-list">
														<li>Name: Nemo II.</li>
														<li>Category: First Class.</li>
														<li>Capacity: 14 Passengers.</li>
														<li>Guide: Naturalist english speaking guide.</li>
														<li>Crew: 6 members: 1 Captain, 1 Helmsman, 1 Sailor, 1 Machinist - Biker, 1 Bar Man - Cabinero, 1 Cook.</li>
														<li>Loa: 21.88 mts.</li>
														<li>Beam: 10.39 mts.</li>
														<li>Draft: 1.50 mts.</li>
														<li>Fresh Water Capacity: 2000 lts.</li>
														<li>Electricity: 12 V, 110 V, 220 V.</li>
														<li>Engines: 2 engines / 200 HP.</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse2">TECHNICAL EQUIPMENT</a>
												</h4>
											</div>
											<div id="collapse2" class="panel-collapse collapse">
												<div class="panel-body">
													<ul class="content-list tick-list">
														<li>Ice maker.</li>
														<li>Freezer.</li>
														<li>Water maker.</li>
														<li>Hot water.</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse3">SECURITY</a>
												</h4>
											</div>
											<div id="collapse3" class="panel-collapse collapse">
												<div class="panel-body">
													<ul class="content-list tick-list">
														<li>2 Life rafts.</li>
														<li>1 EPIRB (Satellite locator).</li>
														<li>1 SART (Radio Beacon).</li>
														<li>2 Waterproof VHF.</li>
														<li>20 Life vests.</li>
														<li>HF & VHF.</li>
														<li>2 Mobile VHF.</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-group">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" href="#collapse4">ACCOMMODATION</a>
												</h4>
											</div>
											<div id="collapse4" class="panel-collapse collapse">
												<div class="panel-body">
													<ul class="content-list tick-list">
														<li>Cabin 1 and cabin 2: Double bed (only for couples).</li>
														<li>Cabin 3 and cabin 4: Lower single bed and double upper bed.</li>
														<li>Cabin 5: 1 Lower double bed and 1 upper single bed.</li>
														<li>Cabin 6-7: 1 Lower single bed and 1 upper single bed.</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
                                    <h3 class="header-box">DECK PLAN</h3>
                                    <a class="fancybox" data-fancybox="images" href="img/nemoII/plan-nemo-II.jpg" title="Deck Plan Nemo II Galapagos Cruises">
										<span class="img-holder">
											<img src="img/nemoII/plan-nemo-II.jpg" title="Plano Nemo II" height="467" width="700" alt="Plano Nemo II">
										</span>
                                    </a>
                                </div>
                            </div>
                        </div>
						<!-- accomodation tab content -->
						<div role="tabpanel" class="tab-pane" id="tab03">
							<h3 class="header-box">NEMO I GALAPAGOS CRUISE ACCOMMODATION</h3>
							<p>
								The NEMO II galapagos island cruise accommodation has been recently renovated in 2016.
								The NEMO II Galapagos cruise accommodation features a modern bathroom and updated bed
								features. The NEMO II galapagos island cruise accommodation has several different cabin
								options to appeal to all sorts of groups. There are two cabins aboard the NEMO II
								Galapagos cruise that is only for couples with one double bed. There are additional 2
								NEMO II Galapagos cruise accommodation that has a lower single bed and a double upper
								bed that is typically for 2 adults but can also accommodate a 3rd child. All of the NEMO
								II galapagos island cruise accommodation has an en suite bathroom and ample space to
								store your luggage. All the NEMO II Galapagos cruise accommodation is suitable for
								whether you are traveling alone, with a companion or a group. All NEMO Galapagos cruises
								are available for also a charter option if you chose to have the entire NEMO Galapagos
								islands cruises to yourself. On all NEMO Galapagos cruises we make sure that our
								passengers have a comfortable stay with all the necessities to have a luxury NEMO II
								Galapagos cruise. On your NEMO II Galapagos cruise you will have several daily
								excursions and you will be grateful to return to a hospitable NEMO II galapagos island
								cruise accommodation and have the chance to relax and take a hot shower. Then you
								venture out into the social areas of your NEMO II Galapagos cruise to either grab a meal
								or socialize with your fellow passengers. All NEMO Galapagos cruises are an intimate
								experience that you share with a few passengers island hopping on a floating catamaran.
							</p>
							<div class="row mt-50">
								<div class="col-md-6 slideInLeft">
										<img src="img/nemoII/images/big/NEMO_II-18.jpg" height="627" width="960" alt="image description">
								</div>
								<div class="col-md-6 text-block slideInRight mt-20">
									<div class="centered">
										<h3 class="intro-heading fs25">CABINS 1 and 2 WITH DOUBLE BED (ONLY FOR COUPLES)</h3>
										<ul class="content-list tick-list">
											<li>Individual Air Conditioning.</li>
											<li>Private bathroom.</li>
											<li>Reading light.</li>
										</ul>
										<a href="#" class="btn btn-primary btn-lg cabins">Check Availability</a>
									</div>
								</div>
							</div>
							<div class="row mt-50">
								<div class="col-md-6 text-block slideInRight mt-20">
									<div class="centered">
										<h3 class="intro-heading fs25">CABINS 3 and 4 WITH LOWER SINGLE BED AND DOUBLE UPPER BED</h3>
										<ul class="content-list tick-list">
											<li>Individual Air Conditioning.</li>
											<li>Private bathroom.</li>
											<li>Reading light.</li>
										</ul>
										<a href="#" class="btn btn-primary btn-lg cabins">Check Availability</a>
									</div>
								</div>
                                <div class="col-md-6 slideInLeft">
									<img src="img/nemoII/images/big/NEMO_II-21.jpg" height="627" width="960" alt="image description">
								</div>
							</div>
							<div class="row mt-50">
								<div class="col-md-6 slideInLeft">
									<img src="img/nemoII/images/big/NEMO_II-22.jpg" height="627" width="960" alt="image description">
								</div>
								<div class="col-md-6 text-block slideInRight mt-20">
									<div class="centered">
										<h3 class="intro-heading fs25">CABINS 5 WITH 1 LOWER DOUBLE BED AND 1 UPPER SINGLE BED</h3>
										<ul class="content-list tick-list">
											<li>Individual Air Conditioning.</li>
											<li>Private bathroom.</li>
											<li>Reading light.</li>
										</ul>
										<a href="#" class="btn btn-primary btn-lg cabins">Check Availability</a>
									</div>
								</div>
							</div>
							<div class="row mt-50">
								<div class="col-md-6 text-block slideInRight mt-20">
									<div class="centered">
										<h3 class="intro-heading fs25">CABINS 6 and 7  WITH 1 LOWER SINGLE BED AND 1 UPPER SINGLE BED</h3>
										<ul class="content-list tick-list">
											<li>Individual Air Conditioning.</li>
											<li>Private bathroom.</li>
											<li>Reading light.</li>
										</ul>
										<a href="#" class="btn btn-primary btn-lg cabins">Check Availability</a>
									</div>
								</div>
								<div class="col-md-6 slideInLeft">
									<img src="img/nemoII/images/big/NEMO_II-24.jpg" height="627" width="960" alt="image description">
								</div>
							</div>
							<p class="mt-50">
								Choosing the perfect vacation can be a difficult decision but embarking with veterans in
								the industry, NEMO Galapagos cruises, has been providing a total experience for the past
								few decades. Hopefully, that puts your worries and concerns at ease knowing that your
								NEMO Galapagos islands cruises are in good hands. NEMO II galapagos cruise accommodation
								is in excellent conditions after being renovated and there is plenty of space to make it
								your own. Making sure that the NEMO II Galapagos cruise accommodation exceeds
								expectations is crucial when making your decision to take your vacation with NEMO
								Galapagos cruises. The NEMO II galapagos island cruise accommodation is a beautiful
								cabin that is a sanctuary when you need rest and relaxation. Combining the intimate
								surrounding of a catamaran with the luxury of NEMO II Galapagos cruise accommodation
								this is an unbeatable vacation. NEMO Galapagos islands cruises takes a lot of pride in
								their vacations that they provide to interested passengers year-round and our NEMO II
								galapagos island cruise accommodation does not fall short of providing a comfortable
								stay for the duration of their vacation. NEMO Galapagos islands cruise adheres to all
								the expectations you may have for your trip by not only providing excellent NEMO II
								galapagos island cruise accommodation but with a professional crew, guide, daily meals
								and excursions. Put your curiosity to the test and take a NEMO Galapagos islands cruises.
								The NEMO II galapagos island cruise accommodation is a testament of our luxury services.
							</p>

						</div>
                        <!-- itinerary tab content -->
						<div role="tabpanel" class="tab-pane" id="tab04">
							<h3 class="header-box">NEMO II GALAPAGOS CRUISE ITINERARIES</h3>
							<p>
								The itinerary of the NEMO II GALAPAGOS CRUISES is separated between the north and the
								south, offering a spectacular vacation. The cruise itinerary NEMO II GALAPAGOS CRUISES
								has been updated to also add options of 8-5-4 days.
							</p>
							<p>
								The ideal of the Galapagos Islands is the weather! Having the convenience of being
								located in the equator allows all the availability of Galapagos cruises to be in service
								throughout the year. Each time you choose to visit one of our cruise itineraries through
								the Galapagos Islands you can meet your needs. Our availability of NEMO II GALAPAGOS
								CRUISES has the option of an 8-5-4 day cruise. The availability of the NEMO II GALAPAGOS
								CRUISES during 8 days is from Sunday to Sunday. The availability of the NEMO II Galápagos
								cruise for a 5 or 4 day cruise depends on the itinerary and starts on Sunday or half of
								the week. The availability of Galapagos cruises has the advantage of being able to take
								vacations at any time throughout the year and attract the demographics of the whole world.
							</p>
							<p>
								The itineraries of the NEMO II GALAPAGOS CRUISE that sails is detailed next:
							</p>
							<div class="row mt-20">
								<div class="col-sm-6">
									<img src="img/nemoI/itinerary/itinerary-A-nemo-I-galapagos-cruise.jpg" title="Itinerary Norte Nemo I Galapagos Cruise" alt="Itinerary Norte Nemo I Galapagos Cruise">
									<a class="btn btn-primary btn-lg cabins"  data-toggle="modal" data-target="#north-8days">
										ITINERARY 8 DAYS
									</a>
									<a class="btn btn-primary btn-lg cabins"  data-toggle="modal" data-target="#north-4days">
										ITINERARY 4 DAYS
									</a>
									<a class="btn btn-primary btn-lg cabins"  data-toggle="modal" data-target="#north-5days">
										ITINERARY 5 DAYS
									</a>
								</div>
								<div class="col-sm-6">
									<img src="img/nemoI/itinerary/itinerary-B-nemo-I-galapagos-cruise.jpg" title="Itinerary Sur Nemo I Galapagos Cruise" alt="Itinerary Norte Nemo I Galapagos Cruise">
									<a class="btn btn-primary btn-lg cabins"  data-toggle="modal" data-target="#south-8days">
										ITINERARY 8 DAYS
									</a>
									<a class="btn btn-primary btn-lg cabins"  data-toggle="modal" data-target="#south-4days">
										ITINERARY 4 DAYS
									</a>
									<a class="btn btn-primary btn-lg cabins"  data-toggle="modal" data-target="#south-5days">
										ITINERARY 5 DAYS
									</a>
								</div>
							</div>

							<h2 class="header-box">NEMO II GALAPAGOS CRUISE AVAILABILITY</h2>

							<p>
								The availability of our NEMO II GALAPAGOS CRUISES can be any day, any month and let our
								talented customer service agents see how we can accommodate ourselves. Let us know what
								dates fit your schedule and we'll take a look at the availability of our Galapagos
								cruises to see which one fits best. If you are only interested in the availability of
								NEMO III GALAPAGOS CRUISES, we will see which of the NEMO II GALAPAGOS CRUISES itineraries
								best fits your selected dates.
							</p>
							<p>
								If, for some reason, the NEMO II GALAPAGOS CRUISES availability does not correspond to
								its opening dates and the itinerary of NEMO II GALAPAGOS CRUISES, then we can venture to
								our other available cruises in Galapagos and I am sure we can find a vacation that suits
								perfectly with your preferences with any of the itineraries of NEMO GALAPAGOS CRUISES.
								Get in touch with us as soon as you are ready to book a vacation and let us have a look
								at the availability of NEMO II GALAPAGOS CRUISES. The NEMO GALAPAGOS cruises on the
								islands are the leaders in the industry and will give you an unforgettable trip.
							</p>
							<div class="page-content">
								<div class="container">
									<div class="ibox" ng-app="AvailableApp">
										<div class="ibox-content" ng-if="yachts !== ''" ng-controller="mainController">
											<?php
                                include_once "php/table-nemo-availability.php";
                                include_once "php/form-modal-availability.php";
                            ?>
										</div>
									</div>
								</div>
							</div>
							<span class="header-box text-center">FOR INFORMATION ABOUT RATES AND AVAILABILITY</span>
							<span class="header-box text-center">PLEASE FEEL FREE TO CONTACT US</span>
							<a class="btn btn-primary btn-lg cabins"  href="#">
								CONTACT US
							</a>
						</div>
					</div>
				</div>

				<!-- GALERIA IMAGENES NEMO I -->
				<aside class="recent-block recent-gray recent-wide-thumbnail">
					<div class="container">
						<h2 class="text-center text-uppercase">NEMO II - IMAGE GALLERY</h2>
                        <ul class="row gallery-list has-center">
							<!--BOTE -->
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-1.jpg" title="Nemo II Galapagos Cruises The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-1.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-2.jpg" title="Nemo II Galapagos Cruises  The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-2.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-3.jpg" title="Nemo II Galapagos Cruises The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-3.jpg" title="Nemo II The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-4.jpg" title="Nemo II Galapagos Cruises  The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-4.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-5.jpg" title="Nemo II Galapagos Cruises  The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-5.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-6.jpg" title="Nemo II Galapagos Cruises  The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-6.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-7.jpg" title="Nemo II Galapagos Cruises  The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-7.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-8.jpg" title="Nemo II Galapagos Cruises  The Boat">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-8.jpg" title="Nemo II  The Boat" height="247" width="370" alt="Nemo II  The Boat">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text"> The Boat</span>
											</span>
										</span>
								</a>
							</li>
							<!--AREA SOCIAL-->
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-9.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-9.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-10.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-10.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-11.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-11.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-12.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-12.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-13.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-13.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-14.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-14.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-15.jpg" title="Nemo II Galapagos Cruises Social Areas">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-15.jpg" title="Nemo II Social Areas" height="247" width="370" alt="Nemo II Social Areas">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Social Areas</span>
											</span>
										</span>
								</a>
							</li>
							<!--BAÑOS-->
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-16.jpg" title="Nemo II Galapagos Cruises Bathroom">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-16.jpg" title="Nemo II Bathroom" height="247" width="370" alt="Nemo II Bathroom">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Bathroom</span>
											</span>
										</span>
								</a>
							</li>
							<li class="col-sm-6">
								<a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-17.jpg" title="Nemo II Galapagos Cruises Bathroom">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-17.jpg" title="Nemo II Bathroom" height="247" width="370" alt="Nemo II Bathroom">
										</span>
									<span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Bathroom</span>
											</span>
										</span>
								</a>
							</li>
							<!--CABINAS-->
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-18.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-18.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-19.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-19.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-20.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-20.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-21.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-21.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-22.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-22.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-23.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-23.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>
							<li class="col-sm-6">
                                <a class="fancybox" data-fancybox-group="group" href="img/nemoII/images/big/NEMO_II-24.jpg" title="Nemo II Galapagos Cruises Cabins">
										<span class="img-holder">
											<img src="img/nemoII/images/NEMO_II-24.jpg" title="Nemo II Cabins" height="247" width="370" alt="Nemo II Cabins">
										</span>
                                    <span class="caption">
											<span class="centered">
												<strong class="title">NEMO II</strong>
												<span class="sub-text">Cabins</span>
											</span>
										</span>
                                </a>
                            </li>

                        </ul>
					</div>
				</aside>
			</main>
		</div>
	<!--MODALES-->
	<!--8 dias Norte-->
	<div id="north-8days" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">NORTH NEMO II GALAPAGOS CRUISE ITINERARY A 8 DAYS </h4>
				</div>
				<div class="modal-body">
					<div id="myCarousel" class="carousel slide" data-interval="false">
						<!-- Indicators -->
						<!--<ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>-->

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<!--DIA 1-->
							<div class="item active row">
								<div class="col-xs-12">
									<h3>DAY 1 - SUNDAY | BALTRA - NORTH SEYMOUR</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<h4>Welcome</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>North Seymour Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 2-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 2 - MONDAY | SANTA CRUZ: HIGHLANDS - CHARLES DARWIN STATION</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Highlands</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>

										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Charles Darwin Station</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 3-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 3 - TUESDAY | ISABELA: MORENO POINT - URBINA BAY</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Moreno Point</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Urbina Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 4-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 4 - WEDNESDAY | ISABELA: TAGUS COVE - FERNANDINA: ESPINOZA POINT</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Tagus Cove</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Fernandina Island: Espinoza Point</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 5-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 5 - THURSDAY | SANTIAGO: EGAS PORT - BUCCANEER COVE</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santiago Island: Egas Port</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santiago Island: Buccaneer Cove</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 6-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 6 - FRIDAY | RABIDA ISLAND - SULLIVAN BAY</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Rabida Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
										<div class="col-xs-6 actividades">
											<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
											<h4>Caminata</h4>
										</div>
										<div class="col-xs-6 actividades">
											<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
											<h4>Kayak</h4>
										</div>
										<div class="col-xs-6 actividades">
											<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
											<h4>Panga Ride</h4>
										</div>
										<div class="col-xs-6 actividades">
											<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
											<h4>Snorkel</h4>
										</div>
									</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santiago Island: Sullivan Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 7-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 7 - SATURDAY | GENOVESA: BARRANCO - DARWIN BAY  </h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Genovesa Island: Barranco</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Genovesa Island: Darwin Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 8-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>DAY 8 - SUNDAY | DAPHNE ISLAND - BALTRA</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Daphne Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/yatch.svg" title="Navegación" alt="Navegación">
												<h4>Circum Navegation</h4>
											</div>
										</div>

									</div>
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
										</div>
										<p>Activities</p>
									</div>
									<div class="row">
										<div class="col-xs-6 actividades">
											<h4>Closing</h4>
										</div>
									</div>
									</div>
								</div>
							</div>
						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"v style="transform: rotate(0deg);"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" style="transform: rotate(0deg);"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default f-left" data-dismiss="modal">Close</button>
					<div class="btn-holder">
						<a href="#" class="btn btn-lg btn-info f-right">BOOK NOW</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!--4 dias Norte-->
	<div id="north-4days" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">NORTH NEMO I GALAPAGOS CRUISE ITINERARY A 4 DAYS </h4>
				</div>
				<div class="modal-body">
					<div id="myCarousel2" class="carousel slide" data-interval="false">
						<!-- Indicators -->
						<!--<ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>-->

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<!--DIA 1-->
							<div class="item active row">
								<div class="col-xs-12">
									<h3>Day 1 - MONDAY | BALTRA - NORTH SEYMOUR</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>North Seymour Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 2-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 2 - TUESDAY | GENOVESA: DARWIN BAY - PRINCE PHILLIP'S STEPS</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Genovesa Island: Darwin Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Genovesa Island: Prince Phillip's Steps</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 3-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 3 - WEDNESDAY | BARTOLOME - CHINESE HAT</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Bartolome Island: Darwin Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santiago Island: Chinese Hat</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 4-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 4 - THURSDAY | SANTA CRUZ: HIGHLANDS:  - BREEDING CENTER FAUSTO LLERENA</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Highlands</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Breeding Center Fausto Llerena</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel2" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" style="transform: rotate(0deg);"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel2" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" style="transform: rotate(0deg);"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default f-left" data-dismiss="modal">Close</button>
					<div class="btn-holder">
						<a href="#" class="btn btn-lg btn-info f-right">BOOK NOW</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!--5 dias Norte-->
	<div id="north-5days" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">NORTH NEMO I GALAPAGOS CRUISE ITINERARY A 8 DAYS </h4>
				</div>
				<div class="modal-body">
					<div id="myCarousel3" class="carousel slide" data-interval="false">
						<!-- Indicators -->
						<!--<ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>-->

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<!--DIA 1-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 1 - THURSDAY | BALTRA - SANTA CRUZ: HIGHLANDS - BREEDING CENTER FAUSTO LLERENA</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Highlands</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Breeding Center Fausto Llerena</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 2-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 2 - FRIDAY | ISABELA: MORENO POINT - URBINA BAY</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Moreno Point</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Urbina Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 3-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 3 - SATURDAY | FERNANDINA: ESPINOZA POINT - ISABELA: VICENTE ROCA POINT</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Fernandina Island: Espinoza Point</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Isabela Island:Vicente Roca Point</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 4-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 4 - SUNDAY | SANTIAGO: EGAS PORT - ESPUMILLA BEACH - BUCCANEER COVE</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santiago Island: Egas Port - Espumilla Beach</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santiago Island: Buccaneer Cove</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 5-->
							<div class="item row">
								<div class="col-xs-12">
									<h3>Day 5 - MONDAY | DAPHNE ISLAND - BALTRA</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Daphne Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel3" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"v style="transform: rotate(0deg);"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel3" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" style="transform: rotate(0deg);"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default f-left" data-dismiss="modal">Close</button>
					<div class="btn-holder">
						<a href="#" class="btn btn-lg btn-info f-right">BOOK NOW</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!--8 dias Sur-->
	<div id="south-8days" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">NORTH NEMO I GALAPAGOS CRUISE ITINERARY A 8 DAYS </h4>
				</div>
				<div class="modal-body">
					<div id="myCarousel4" class="carousel slide" data-interval="false">
						<!-- Indicators -->
						<!--<ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>-->

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<!--DIA 1-->
							<div class="item active row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 1 - MONDAY | Baltra - Bachas Beach</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<h4>Welcome</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Bachas Beach</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 2-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 2 - TUESDAY | Isabela: Las Tintoreras - Arnaldo Tupiza Breeding Center</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Las Tintoreras</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Arnaldo Tupiza Tortoise Breeding Center</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 3-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 3 - WEDNESDAY | Floreana: Cormorant Point - Devil's Crown - Post Office Bay</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Floreana Island: Cormorant Point - Post Office Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Floreana Island: Devil's Crown</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 4-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 4 - THURSDAY | Española: Suarez Point - Gardner Bay – Osborn Islet</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Española Island: Suarez Point – Osborn Islet</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Española Island: Gardner Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 5-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 5 - FRIDAY | Baltra - Santa Cruz: El Chato Tortoise Reserve - Breeding Center Arnaldo Tupiza</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: El Chato Tortoise Reserve</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Breeding Center Arnaldo Tupiza</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 6-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 6 - SATURDAY | San Cristobal: Pitt Point – Brujo Hill</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>San Cristobal Island: Pitt Point</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>San Cristobal Island: Brujo Hill</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 7-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 7 - SUNDAY | Santa Fe - South Plaza</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Fe Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>South Plaza Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 8-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 8 - MONDAY | Black Turtle Cove - Baltra</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Black Turtle Cove</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
										</div>

									</div>
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
										</div>
										<p>Activities</p>
									</div>
									<div class="row">
										<div class="col-xs-6 actividades">
											<h4>Closing</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel4" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"v style="transform: rotate(0deg);"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel4" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" style="transform: rotate(0deg);"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default f-left" data-dismiss="modal">Close</button>
					<div class="btn-holder">
						<a href="#" class="btn btn-lg btn-info f-right">BOOK NOW</a>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!--5 dias Sur-->
	<div id="south-5days" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">NORTH NEMO I GALAPAGOS CRUISE ITINERARY A 8 DAYS </h4>
				</div>
				<div class="modal-body">
					<div id="myCarousel5" class="carousel slide" data-interval="false">
						<!-- Indicators -->
						<!--<ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>-->

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<!--DIA 1-->
							<div class="item active row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 1 - MONDAY | Baltra - Bachas Beach</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<h4>Welcome</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: Bachas Beach</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 2-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 2 - TUESDAY | Isabela: Las Tintoreras - Arnaldo Tupiza Breeding Center</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Las Tintoreras</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Isabela Island: Arnaldo Tupiza Tortoise Breeding Center</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 3-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 3 - WEDNESDAY | Floreana: Cormorant Point - Devil's Crown - Post Office Bay</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Floreana Island: Cormorant Point - Post Office Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/balsa-de-goma.svg" title="Panga Ride" alt="Panga Ride">
												<h4>Panga Ride</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Floreana Island: Devil's Crown</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 4-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 4 - THURSDAY | Española: Suarez Point - Gardner Bay – Osborn Islet</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Española Island: Suarez Point – Osborn Islet</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Española Island: Gardner Bay</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/kayac.svg" title="Kayak" alt="Kayak">
												<h4>Kayak</h4>
											</div>
											<div class="col-xs-6 actividades">
												<img src="img/icons/esnorquel.svg" title="Snorkel" alt="Snorkel">
												<h4>Snorkel</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--DIA 5-->
							<div class="item row">
								<div class="col-xs-12">
									<h3 class="text-uppercase">Day 5 - FRIDAY | Baltra - Santa Cruz: El Chato Tortoise Reserve - Breeding Center Arnaldo Tupiza</h3>
								</div>
								<div class="col-xs-6">
									<img src="img/nemoI/images/big/NEMO_I-1.jpg" alt="Los Angeles">
								</div>
								<div class="col-xs-6">
									<div class="holder">
										<div class="description">
											<h4>Santa Cruz Island: El Chato Tortoise Reserve</h4>
											<p>Activities</p>
										</div>
										<div class="row">
											<div class="col-xs-6 actividades">
												<img src="img/icons/excursionista.svg" title="Caminata" alt="Caminata">
												<h4>Caminata</h4>
											</div>
										</div>
									</div>
									<div class="holder">
										<div class="description">
											<h4>Baltra Island</h4>
										</div>
										<p>Activities</p>
									</div>
									<div class="row">
										<div class="col-xs-6 actividades">
											<h4>Closing</h4>
										</div>
									</div>
								</div>
							</div>

						</div>
						<!-- Left and right controls -->
						<a class="left carousel-control" href="#myCarousel5" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"v style="transform: rotate(0deg);"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#myCarousel5" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" style="transform: rotate(0deg);"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default f-left" data-dismiss="modal">Close</button>
					<div class="btn-holder">
						<a href="#" class="btn btn-lg btn-info f-right">BOOK NOW</a>
					</div>
				</div>
			</div>

		</div>
	</div>

	<!--FOOTER -->
    <footer id="footer">
        <div class="container">
            <!-- newsletter form -->
            <form action="php/subscribe.php" id="signup" method="post" class="newsletter-form">
                <fieldset>
                    <div class="input-holder">
                        <input type="email" class="form-control" placeholder="Email Address" name="subscriber_email" id="subscriber_email">
                        <input type="submit" value="GO">
                    </div>
                    <span class="info" id="subscribe_message">To receive news, updates and tour packages via email.</span>
                </fieldset>
            </form>
            <!-- footer links -->
            <div class="row footer-holder">
                <nav class="col-sm-4 col-lg-3 footer-nav active">
                    <h3>FLEET</h3>
                    <ul class="slide">
                        <li><a href="nemo-I.html">Nemo I</a></li>
                        <li><a href="#">Nemo II</a></li>
                        <li><a href="#">Nemo III</a></li>

                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav">
                    <h3>Destinations</h3>
                    <ul class="slide">
                        <li><a href="#">Galapago Islands</a></li>
                        <li><a href="#">Ecuador Andes</a></li>
                        <li><a href="#">Ecuador Amazon</a></li>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-3 footer-nav">
                    <h3>Other Destinations</h3>
                    <ul class="slide">
                        <li><a href="#">Perú - MachuPicchu</a></li>
                        <li><a href="#">Perú - Lima</a></li>
                        <li><a href="#">Bolivia</a></li>
                    </ul>
                </nav>
                <!--<nav class="col-sm-4 col-lg-2 footer-nav">
                    <h3>reservation</h3>
                    <ul class="slide">
                        <li><a href="#">Booking Conditions</a></li>
                        <li><a href="#">My Bookings</a></li>
                        <li><a href="#">Refund Policy</a></li>
                        <li><a href="#">Includes &amp; Excludes</a></li>
                        <li><a href="#">Your Responsibilities</a></li>
                        <li><a href="#">Order a Brochure</a></li>
                    </ul>
                </nav>
                <nav class="col-sm-4 col-lg-2 footer-nav">
                    <h3>ask Entrada</h3>
                    <ul class="slide">
                        <li><a href="#">Why Entrada?</a></li>
                        <li><a href="#">Ask an Expert</a></li>
                        <li><a href="#">Safety Updates</a></li>
                        <li><a href="#">We Help When...</a></li>
                        <li><a href="#">Personal Matters</a></li>
                    </ul>
                </nav>-->
                <nav class="col-sm-4 col-lg-3 footer-nav last">
                    <h3>contact LatinTour</h3>
                    <ul class="slide address-block">
                        <li class="wrap-text"><span class="icon-tel"></span> <a href="#">+1 305 848 7326</a></li>
                        <li class="wrap-text"><span class="icon-tel"></span> <a href="#">+1 604 243 9774</a></li>
                        <li class="wrap-text"><span class="icon-tel"></span> <a href="#">+593 2 2 508 800</a></li>
                        <!--<li class="wrap-text"><span class="icon-tel"></span> <a href="tel:02072077878">+593 2 2 508 800</a></li>-->
                        <li><span class="icon-home"></span> <address>DIEGO DE ALMAGRO N26 - 205 AND LA NINA
                                QUITO EC170135</address></li>
                    </ul>
                </nav>
            </div>
            <!-- social wrap -->
            <ul class="social-wrap">
                <li class="facebook"><a target="_blank" href="https://www.facebook.com/NemoGalapagosCruises/timeline/">
                        <span class="icon-facebook"></span>
                        <strong class="txt">Like Us</strong>
                    </a></li>
                <li class="twitter"><a target="_blank" href="https://twitter.com/NemoGalapagos">
                        <span class="icon-twitter"></span>
                        <strong class="txt">Follow On</strong>
                    </a></li>
                <li class="google-plus"><a target="_blank" href="https://www.youtube.com/user/nemogalapagos">
                        <span class="fa-icon-google-plus"></span>
                        <strong class="txt">Like Us</strong>
                    </a></li>
                <li class="vimeo"><a href="#">
                        <span class="icon-vimeo"></span>
                        <strong class="txt">Share At</strong>
                    </a></li>
                <li class="pin"><a target="_blank" href="https://www.pinterest.com/NemoGalapagos/">
                        <span class="icon-pin"></span>
                        <strong class="txt">Pin It</strong>
                    </a></li>
                <!--<li class="dribble"><a href="#">
                    <span class="icon-dribble"></span>
                    <strong class="txt">Dribbble</strong>
                </a></li>-->
            </ul>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- copyright -->
                        <strong class="copyright"><i class="fa fa-copyright"></i> Copyright 2019 - Nemo Galapagos Cruises - Latintour Cia. Ltda.</strong>
                    </div>
                    <div class="col-lg-6">
                        <ul class="payment-option">
                            <li>
                                <img src="img/footer/visa.png" alt="visa">
                            </li>
                            <li>
                                <img src="img/footer/mastercard.png" height="20" width="33" alt="master card">
                            </li>
                            <!--<li>
                                <img src="img/footer/paypal.png" height="20" width="72" alt="paypal">
                            </li>
                            <li>
                                <img src="img/footer/maestro.png" height="20" width="33" alt="maestro">
                            </li>-->
                            <li>
                                <img src="img/footer/bank-transfer.png" height="20" width="55" alt="bank transfer">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	</div>
	<!-- scroll to top -->
	<div class="scroll-holder text-center">
		<a href="javascript:" id="scroll-to-top"><i class="icon-arrow-down"></i></a>
	</div>
	<!-- jquery library -->
	<script src="vendors/jquery/jquery-2.1.4.min.js"></script>
	<!-- external scripts -->
	<script src="vendors/bootstrap/javascripts/bootstrap.min.js"></script>
	<script src="vendors/jquery-placeholder/jquery.placeholder.min.js"></script>
	<script src="vendors/match-height/jquery.matchHeight.js"></script>
	<script src="vendors/wow/wow.min.js"></script>
	<script src="vendors/stellar/jquery.stellar.min.js"></script>
	<script src="vendors/validate/jquery.validate.js"></script>
	<script src="vendors/waypoint/waypoints.min.js"></script>
	<script src="vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="vendors/jquery-ui/jquery-ui.min.js"></script>
	<script src="vendors/jQuery-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<script src="vendors/fancybox/jquery.fancybox.js"></script>
	<script src="vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="vendors/jcf/js/jcf.js"></script>
	<script src="vendors/jcf/js/jcf.select.js"></script>
	<script src="vendors/retina/retina.min.js"></script>
	<script src="vendors/bootstrap-datetimepicker-master/dist/js/bootstrap-datepicker.js"></script>
	<script src="vendors/sticky-kit/sticky-kit.js"></script>
	<!-- mailchimp newsletter subscriber -->
	<script src="js/mailchimp.js"></script>
	<!--INTEGRATION-->
	<script src="js/integration/angular.min.js"></script>
	<script src="js/integration/appNemo2.js"></script>
	<!-- custom script -->
	<script src="js/sticky-kit-init.js"></script>
	<script src="js/jquery.main.js"></script>
</body>
</html>